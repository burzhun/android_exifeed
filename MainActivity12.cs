using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exifeed;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Util;
using System.Net;

namespace AndroidGesture
{
    [Activity(Label = "MainActivity", MainLauncher = true, Theme = "@style/AppTheme")]
    public class MainActivity12 : ActionBarActivity
    {

        private Toolbar toolbar;
        private RecyclerView recyclerView;
        private RecyclerView.Adapter left_adapter;
        private RecyclerView.LayoutManager layoutManager;
        private DrawerLayout drawer;
        private ActionBarDrawerToggle drawerToggle;
        TextView txtActionBarText;
        TextView txtDescription;
        EditText search;
        ImageView search_icon;
        private List<Post> list;
        WebClient webClient;
        Uri url;
        ListView listView;
        int offset;
        int limit;
        string PostsType;
        string token;
        string api_key;
        int resume = 0;
        Post_adapter adapter = null;
        string prev_type;
        ImageView search_close_icon;
        string[] Titles;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            listView = FindViewById<ListView>(Resource.Id.postListView);
            listView.Scroll += listviewscrolled;
            webClient = new WebClient();
            offset = 0;
            limit = 10;
            PostsType = "top";
            api_key = "sdsgre345345hkb5kh345kj345";
            string url_user = UrlManager.domain + "?api_key=" + api_key;
            token = UserParameters.retrieveset("token");
            if (token != null)
            {
                url_user += "&check_token535345=" + token;
                Uri userToken = new Uri(url_user);
                webClient.DownloadDataAsync(userToken);
                webClient.DownloadDataCompleted += check_user;
            }
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            DownloadPosts("");

            txtActionBarText = FindViewById<TextView>(Resource.Id.txtActionBarText);
            search = FindViewById<EditText>(Resource.Id.search_field);
            search_icon = FindViewById<ImageView>(Resource.Id.search_icon);
            search_close_icon = FindViewById<ImageView>(Resource.Id.search_close_icon);
            search_icon.Click += delegate {
                search_icon.Visibility = ViewStates.Gone;
                search.Visibility = ViewStates.Visible;
                txtActionBarText.Visibility = ViewStates.Gone;
                search_close_icon.Visibility = ViewStates.Visible;
            };
            toolbar.Click += delegate
            {
                if (search.Text == "")
                {
                    close_search();
                }
            };
            search_close_icon.Click += delegate
            {
                close_search();
            };
            search.KeyPress += (o, e) =>
            {
                if (e.KeyCode == Keycode.Enter && e.Event.Action == KeyEventActions.Down)
                {
                    if (search.Text != "")
                    {
                        prev_type = PostsType;
                        PostsType = "search";
                        DownloadPosts(PostsType);
                    }
                }
                else
                {
                    e.Handled = false;
                }
            };
            
        }

        private void close_search()
        {
            search_icon.Visibility = ViewStates.Visible;
            search.Visibility = ViewStates.Gone;
            txtActionBarText.Visibility = ViewStates.Visible;
            search_close_icon.Visibility = ViewStates.Gone;
            Console.WriteLine(prev_type);
            PostsType = prev_type;
            offset = 0;
            DownloadPosts(PostsType);
        }

        private void listviewscrolled(object sender, AbsListView.ScrollEventArgs e)
        {
            if (e.View.LastVisiblePosition + 1 == offset + limit)
            {
                Console.WriteLine((offset + limit).ToString());
                DownloadPosts(PostsType, offset + limit);
                offset += limit;
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (resume == 0)
            {
                resume++;
                return;
            }
            if (token == null && UserParameters.retrieveset("token") != null)
            {
                CreateMenu(true, true);
                string url_user = UrlManager.domain + "?api_key=" + api_key;
                // UserParameters.saveset("token", null);
                token = UserParameters.retrieveset("token");
                url_user += "&check_token535345=" + token;
                Uri userToken = new Uri(url_user);
                webClient.DownloadDataAsync(userToken);
                webClient.DownloadDataCompleted += check_user;
                offset = 0;
                DownloadPosts(PostsType);
            }
            if (UserParameters.retrieveset("userimage_changed") != null)
            {
                CreateMenu(true, true);
            }

        }
        public void check_user(object sender, DownloadDataCompletedEventArgs e)
        {
            RunOnUiThread(() =>
            {
                string json = Encoding.UTF8.GetString(e.Result);
                if (json == "nope")
                {
                    CreateMenu(false,true);
                    UserParameters.saveset("token", null);
                }
                else
                {
                   // ViewGroup.MarginLayoutParams params1 = (ViewGroup.MarginLayoutParams)menuListView.LayoutParameters;
                    //params1.SetMargins(0, 130, 0, 0);
                    Console.WriteLine(json);
                    //leftMenu.LayoutParameters = params1;
                 //   RelativeLayout userLayout = FindViewById<RelativeLayout>(Resource.Id.userblockLayout);
                    //userLayout.Visibility = ViewStates.Visible;
                    User user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(json);
                    UserParameters.saveset("username", user.username);
                    UserParameters.saveset("shorten_post", user.shorten_post);
                    UserParameters.saveset("userimage", user.image);
                    UserParameters.saveset("user_id", user.id);
                    CreateMenu(true, true);
                    //  ImageView userImage = FindViewById<ImageView>(Resource.Id.userImage);
                    // Koush.UrlImageViewHelper.SetUrlDrawable(userImage, user.image);
                    // TextView userName = FindViewById<TextView>(Resource.Id.userName);
                    // userName.Text = user.username;

                }
            });
        }
        private void DownloadPosts(string type, int offset1 = 0, int limit1 = 10)
        {
            WebClient webClient = new WebClient();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (type != "")
            {
                dict.Add("type", type);
            }
            if (offset1 != 0)
            {
                dict.Add("offset", offset1.ToString());
            }
            if (limit1 != 10)
            {
                dict.Add("limit", limit1.ToString());
            }
            if (type == "search")
            {
                dict.Add("search", search.Text);
            }
            string json = UrlManager.PostRequest("posts", dict);
            if (json != "error" && json != "")
            {
                listView.Visibility = ViewStates.Visible;
                if (offset1 == 0)
                {
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                    adapter = new Post_adapter(list, this);
                    listView.Adapter = adapter;
                    listView.SetWillNotCacheDrawing(false);
                }
                else
                {
                    List<Post> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                    list.AddRange(list2);
                    //adapter = new Post_adapter(list, this);
                    adapter.NotifyDataSetChanged();
                    //listView.Adapter = adapter;
                }
            }
            else if (json == "error")
            {
                txtActionBarText.Text = "No posts found";
                listView.Visibility = ViewStates.Invisible;
            }

        }


        public void LeftMenuClick(string text)
        {
            drawer.CloseDrawer(recyclerView);
            if (text == "Login")
            {
                Intent intent = new Intent(this, typeof(Login));
                StartActivity(intent);
                return;
            }
            if (text == "Logout")
            {
                UserParameters.saveset("username", null);
                UserParameters.saveset("userimage", null);
                UserParameters.saveset("token", null);
                CreateMenu(false,true);
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("logout", "1");
                UrlManager.PostRequest("", dict);
                string urls = UrlManager.domain + "posts?api_key=" + api_key + "&logout=1";
                offset = 0;
                DownloadPosts(PostsType);
                return;
            }
            if (text == "Add post")
            {
                Intent intent = new Intent(this, typeof(AddPost));
                StartActivity(intent);
                return;
            }
            if (text == "Comments")
            {
                Intent intent = new Intent(this, typeof(MyComments));
                StartActivity(intent);
                return;
            }
            if (text == "About")
            {
                Intent intent = new Intent(this, typeof(About));
                StartActivity(intent);
                return;
            }
            if (txtActionBarText.Text != text)
            {
                close_search();
                PostsType = text;
                offset = 0;
                DownloadPosts(PostsType);
                txtActionBarText.Text = text;
            }
        }
        public void CreateMenu(bool logged,bool update = false)
        {            
            if (!logged)
            {
                Titles = new string[] { "Top", "New", "Best", "Login", "About" };
            }
            else
            {
                Titles = new string[] { "Top", "New", "Best", "Feed", "Saved posts", "Comments", "Add post", "Logout", "About" };
            }
            SetSupportActionBar(toolbar);
            // toolbar.SetNavigationIcon(Resource.Drawable.menu_icon);
            recyclerView = FindViewById<RecyclerView>(Resource.Id.RecyclerView);
            recyclerView.HasFixedSize = true;

            //Make sure that we always have the minimum right margin of 56dp on the navigation drawer
            var minDrawerMargin = (int)Resources.GetDimension(Resource.Dimension.navigation_drawer_min_margin);
            var maxDrawerWidth = Resources.DisplayMetrics.WidthPixels - minDrawerMargin;
            recyclerView.LayoutParameters.Width = Math.Min(recyclerView.LayoutParameters.Width, maxDrawerWidth);

            left_adapter = new DrawerAdapter(Titles,  this);
            recyclerView.SetAdapter(left_adapter);

            layoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);

            drawer = FindViewById<DrawerLayout>(Resource.Id.DrawerLayout);
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var id = item.ItemId;

            //   if (id == Resource.Id.action_settings)
            //   return true;
            return base.OnOptionsItemSelected(item);
        }

        
    }
}
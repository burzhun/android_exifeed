using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Text.RegularExpressions;

namespace AndroidGesture
{
    [Activity(Label = "AddVideo")]
    public class AddVideo : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_video);
            // Create your application here

            EditText videoview = FindViewById<EditText>(Resource.Id.add_post_video_view);
            TextView error = FindViewById<TextView>(Resource.Id.add_video_empty);
            Button add = FindViewById<Button>(Resource.Id.add_video_to_post_button);
            add.Click += delegate {
                error.Visibility = ViewStates.Gone;
                string video = videoview.Text;
                string type = "";
                if (video != "")
                {
                    var match = Regex.Match(video, ".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?.*v=)([^#\\&\\?]*).*");
                    if(match.Success)
                    {
                        video = match.Groups[1].ToString();
                        type = "youtube";
                    }else
                    {
                        int t = 0;
                        int n = 0;
                        if (video.StartsWith("coub.com"))
                        {
                            video = "http://" + video;
                        }
                        if(video.IndexOf("coub.com/embed/") > 0)
                        {
                            t = video.IndexOf("coub.com/embed/");
                            n = "coub.com/embed/".Length;
                        }else
                        {
                            if (video.IndexOf("coub.com/view/") > 0)
                            {
                                t = video.IndexOf("coub.com/view/");
                                n = "coub.com/view/".Length;
                            }
                        }
                        if (t > 0)
                        {
                            if (video.IndexOf('?') > 0)
                            {
                                int t1 = video.IndexOf('?');
                                video = video.Substring(t + n, t1 - t - n);
                            }else
                            {
                                video = video.Substring(t + n);
                            }
                            type = "coub";
                        }else
                        {
                            AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            alert.SetTitle("Your link is wrong");
                            alert.SetPositiveButton("Ok", (senderAlert, args) => {
                            });
                            RunOnUiThread(() => {
                                alert.Show();
                            });
                        }
                    }
                    if (type != "")
                    {
                        Intent intent = new Intent();
                        intent.PutExtra("video", video);
                        intent.PutExtra("type", type);
                        ((Activity)this).SetResult(Result.Ok, intent);
                        ((Activity)this).Finish();
                    }
                    
                }
                else
                {
                    error.Visibility = ViewStates.Visible;
                }
            };
        }
    }
}
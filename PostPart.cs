using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Text;
using System.Text.RegularExpressions;
using Android.Media;
using Com.Google.Android.Exoplayer2.Upstream;
using Com.Google.Android.Exoplayer2.Trackselection;
using Com.Google.Android.Exoplayer2;
using Com.Google.Android.Exoplayer2.Util;
using Com.Google.Android.Exoplayer2.Extractor;
using Com.Google.Android.Exoplayer2.Source;
using Java.IO;
using System.Net;

namespace AndroidGesture
{
    
    class PostPart
    {
        public string type;
        public string id;
        public string title;
        public string image;
        public string video;
        public string audio;
        public static MediaPlayer mp = new MediaPlayer();
        public static int last_played_index = 0;
        static Handler mainHandler = new Handler();
        static IBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        static ITrackSelectionFactory videoTrackSelectionFactory =
            new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        public static TrackSelector trackSelector =
            new DefaultTrackSelector(videoTrackSelectionFactory);
        public static DefaultAllocator a = new DefaultAllocator(true, 8000 * 1024);
        public static ILoadControl loadControl = new DefaultLoadControl(a,500,5000,5000,5000);
        public static IExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        public static ITransferListener bandwidthMeter1 = new DefaultBandwidthMeter();
        public static SimpleExoPlayer player;
        public static IMediaSource videoSource;
        public PostPart(string Type,string ID,string Title,string Image,string Video,string Audio)
        {
            type = Type;
            id = ID;
            title = Title;
            image = Image;
            video = Video;
            audio = Audio;
        }

        public RelativeLayout GetLayout(Context context,int position)
        {
            RelativeLayout rl = new RelativeLayout(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            rl.LayoutParameters = p;
            switch (this.type)
            {
                case "text":
                    {
                        TextView tv = new TextView(context);
                        tv.TextFormatted = Html.FromHtml(this.title);
                        tv.SetTextColor(Color.Black);
                        tv.SetTextSize(Android.Util.ComplexUnitType.Dip, 16);
                        tv.AutoLinkMask = Android.Text.Util.MatchOptions.WebUrls;
                        tv.SetOnClickListener(new TextViewClickListener(context,this.title));
                        rl.AddView(tv);
                    }
                    break;
                case "image":
                    {
                        ImageView im = CreateImageView(image, context);
                        im.SetOnLongClickListener(new PlayButtonLongClickListener(image, context,false));
                        if (im != null)
                        {                            
                            rl.AddView(im);
                        }
                    }   
                    break;
                case "gif":
                    {
                        GetVideoLayout(context, ref rl, this.image, this.video,position);
                    }
                    break;
                case "coub":
                    {
                        if(this.video!=null && this.image != null) {
                            GetVideoLayout(context, ref rl, this.image, this.video,position);
                        }
                    }
                    break;
                case "youtube":
                    {
                        ImageView im = CreateImageView(image, context);
                        RelativeLayout rl1 = new RelativeLayout(context);
                        rl1.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                        if (im != null)
                        {
                            rl1.AddView(im);
                            rl.AddView(rl1);
                            rl.AddView(CreatePlayButton(context));
                            ImageView youtube_icon = new ImageView(context);
                            RelativeLayout.LayoutParams p_y = new RelativeLayout.LayoutParams(96,60);
                            youtube_icon.LayoutParameters = p_y;
                            p_y.AddRule(LayoutRules.AlignParentBottom);
                            p_y.AddRule(LayoutRules.AlignParentLeft);
                            youtube_icon.SetAdjustViewBounds(true);
                            youtube_icon.SetScaleType(ImageView.ScaleType.CenterCrop);
                            youtube_icon.SetImageResource(Resource.Drawable.youtube_icon);
                            rl.AddView(youtube_icon);
                        }
                    }
                    break;
            }
            
            return rl;
        }

        public class PlayButtonClickListener : Java.Lang.Object, View.IOnClickListener
        {
            string url;
            Context c;
            RelativeLayout r1;
            ImageView im1,b;
            int w, h, position;
            long elapsed=0;
            IMediaSource mediaSource;
            IDataSourceFactory dataSourceFactory;
            public PlayButtonClickListener(string url1, Context context,ref RelativeLayout video,ref ImageView im,ref ImageView button,int width,int height, int position1)
            {
                url = url1;
                c = context;
                r1 = video;
                im1 = im;
                b = button;
                w = width;
                h = height;
                position = position1;
                dataSourceFactory = new DefaultHttpDataSourceFactory(
                                    ExoPlayerUtil.GetUserAgent(c, "Exifeed"));
                mediaSource = new ExtractorMediaSource(Android.Net.Uri.Parse(url),
                                dataSourceFactory, extractorsFactory, null, null);
            }
            public void OnClick(View v)
            {
                PostPart.last_played_index = position;
                TextureView v1 = new TextureView(c);
                if (h > 0) v1.LayoutParameters = new ViewGroup.LayoutParams(w, h);
                else
                {
                    v1.LayoutParameters = new ViewGroup.LayoutParams(5,5);
                    v1.LayoutParameters = new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
                    r1.ViewTreeObserver.GlobalLayout += delegate
                    {
                        if (r1.Height > 0)
                        {
                            v1.LayoutParameters = new RelativeLayout.LayoutParams(r1.Width, r1.Height);
                            UserParameters.AddVideoSize(url, r1.Width, r1.Height);
                        }
                    };
                }
                
                r1.AddView(v1);                
                v1.Visibility = ViewStates.Visible;       
                // im1.Visibility = ViewStates.Invisible;
                b.Visibility = ViewStates.Invisible;
                v1.SurfaceTextureAvailable += (o1, e1) =>
                {
                    try
                    {
                        if (player != null)
                        {
                            player.PlayWhenReady = false;
                            player.Stop();
                            player.SeekTo(0);
                            videoSource = null;
                        }
                        player = ExoPlayerFactory.NewSimpleInstance(c, trackSelector, loadControl);
                        player.SetVideoTextureView(v1);                     
                        videoSource = mediaSource;
                        player.AddListener(new ExoListener(ref player));
                        player.Prepare(videoSource);
                        player.Volume = 0.5f;

                    }
                    catch (System.Exception e11)
                    {

                    }
                };
                                                 
                v1.Click += delegate {
                    v1.Visibility = ViewStates.Invisible;
                    b.Visibility = ViewStates.Visible;
                    if(player != null)
                    {
                        elapsed = player.CurrentPosition;
                        player.PlayWhenReady = false;
                        player.Stop();
                    }
                    
                };
                b.Click += delegate {
                    v1.Visibility = ViewStates.Visible;
                    b.Visibility = ViewStates.Invisible;
                    if(player != null)
                    {
                        player.Prepare(videoSource);
                        player.SeekTo(elapsed);
                        player.PlayWhenReady = true;
                    }                    
                };                
            }

        }

        private ImageView CreateImageView(string Url,Context context,VideoView video=null)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);            
            im.LayoutParameters = p;
            im.SetAdjustViewBounds(true);
            im.SetScaleType(ImageView.ScaleType.CenterCrop);
            //Bitmap imageBitmap = GetImageBitmapFromUrl(Url);
            Koush.UrlImageViewHelper.SetUrlDrawable(im, Url);
            //im.SetImageBitmap(imageBitmap);
            im.Click += delegate
            {
                ImageClick(context, this.type, this.id, this.video, this.audio);
            };
            return im;
        }
       
        public ImageView CreatePlayButton(Context context)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(150, 150);
            im.LayoutParameters = p;
            p.AddRule(LayoutRules.CenterInParent);
            im.SetAdjustViewBounds(true);
            im.SetImageResource(Resource.Drawable.play_button);
            return im;
        }
        private void ImageClick(Context context,string type,string id,string video,string audio)
        {
            if(type == "image")
            {
                var intent = new Intent(context, typeof(ImageViewer));
                intent.PutExtra("image_src", this.image);
                context.StartActivity(intent);
            }
            if (type == "gif")
            {
                //Toast.MakeText(context, video, ToastLength.Short).Show();
                 Intent intent = new Intent(context, typeof(Video));
                 intent.PutExtra("video", video);
                 context.StartActivity(intent);
                
            }
            if (type == "youtube")
            {
                string url = "https://youtube.com/watch?v=" + id;
                context.StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse(url)));
            }
            if(type == "coub")
            {
                Intent intent = new Intent(context, typeof(Video));
                intent.PutExtra("video", video);
                intent.PutExtra("audio", audio);
               // Toast.MakeText(context, audio, ToastLength.Short).Show();
                context.StartActivity(intent);
            }
        }

        public RelativeLayout GetVideoLayout(Context context,ref RelativeLayout layout,string image,string video_url,int position)
        {
            RelativeLayout rl1 = new RelativeLayout(context);
            TextureView video = new TextureView(context);
            video.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 300);
            video.Visibility = ViewStates.Gone;
            rl1.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);

            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
            im.LayoutParameters = p1;
            im.SetAdjustViewBounds(true);
            im.SetScaleType(ImageView.ScaleType.CenterCrop);

            ImageView fullscreen_button = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(80,80);
            fullscreen_button.LayoutParameters = p;
            p.AddRule(LayoutRules.AlignParentBottom);
            p.AddRule(LayoutRules.AlignParentRight);
            fullscreen_button.SetAdjustViewBounds(true);
            fullscreen_button.SetScaleType(ImageView.ScaleType.CenterCrop);
            fullscreen_button.SetImageResource(Resource.Drawable.full_screen);
            fullscreen_button.Click += delegate
            {
                Intent intent = new Intent(context, typeof(VideoFullscreen)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("video", video_url);
                context.StartActivity(intent);
            };

            //Bitmap imageBitmap = GetImageBitmapFromUrl(Url);
            Koush.UrlImageViewHelper.SetUrlDrawable(im, image);
            ImageView button = CreatePlayButton(context);
            im.Focusable = false;
            im.Clickable = true;
            im.SetOnLongClickListener(new PlayButtonLongClickListener(video_url,context));
            if (UserParameters.video_sizes.ContainsKey(video_url))
            {
                var array = UserParameters.GetVideoSize(video_url);
                int width = array[0];
                int height = array[1];
                im.SetOnClickListener(new PlayButtonClickListener(video_url, context, ref rl1, ref im, ref button, width, height,position));
            }
            else
            {
                im.ViewTreeObserver.GlobalLayout += delegate {
                    UserParameters.AddVideoSize(video_url, im.Width, im.Height);
                };
                im.SetOnClickListener(new PlayButtonClickListener(video_url, context, ref rl1, ref im, ref button, im.Width, im.Height,position));
            }
            rl1.AddView(im);
            layout.AddView(rl1);
            layout.AddView(button);
            layout.AddView(fullscreen_button);
            return rl1;
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new System.Net.WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }
    }

    internal class PlayButtonLongClickListener : Java.Lang.Object,  View.IOnLongClickListener
    {
        private string video_url;
        Context c;
        bool isVideo;
        public PlayButtonLongClickListener(string video_url,Context c1,bool isvideo = true)
        {
            this.video_url = video_url;
            c = c1;
            isVideo = isvideo;
        }

        public bool OnLongClick(View v)
        {
            string[] items = { isVideo ?"Save video" : "Save Image",  "Cancel" };
            using (var dialogBuilder = new AlertDialog.Builder(c))
            {
                dialogBuilder.SetTitle("Add Image");
                dialogBuilder.SetItems(items, (d, args) => {
                    //Take photo
                    if (args.Which == 0)
                    {
                        System.ComponentModel.BackgroundWorker comments_worker = new System.ComponentModel.BackgroundWorker();
                        comments_worker.DoWork += delegate
                        {
                            string[] ar = this.video_url.Split('/');
                            string last = ar[ar.Length - 1];
                            if (!last.Contains("."))
                            {
                                last += isVideo ? ".mp4" : ".jpg";
                            }
                            string filename =( isVideo ? "Exifeed_video_" : "Exifeed_image") + Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 5) + "_" + last;
                            DownloadFromUrl(this.video_url, filename);
                        };
                        comments_worker.RunWorkerAsync();                        
                    }
                });

                dialogBuilder.Show();
            }
            return true;
        }

        public void DownloadFromUrl(string DownloadUrl, string fileName)
        {
            try
            {
                System.Console.WriteLine(DownloadUrl + "   " + fileName);
                File root = Android.OS.Environment.ExternalStorageDirectory;
                File dir = new File(root.AbsolutePath + "/exifeed");
                if (dir.Exists() == false)
                {
                    dir.Mkdirs();
                }
                File file = new File(dir, fileName);
                WebClient myClient = new WebClient();
                byte[] bytes = myClient.DownloadData(DownloadUrl);
                FileOutputStream fos = new FileOutputStream(file);
                fos.Write(bytes);
                fos.Flush();
                fos.Close();
                Android.Net.Uri uri = Android.Net.Uri.Parse(file.ToURI().ToString());
                c.SendBroadcast(new Intent(Intent.ActionMediaScannerScanFile, uri));
            }
            catch (IOException e)
            {
            }

        }
    }

    

    internal class ExoListener : Java.Lang.Object, IExoPlayerEventListener
    {
        SimpleExoPlayer p;
        public ExoListener(ref SimpleExoPlayer player)
        {
            p = player;
        }
        public void OnLoadingChanged(bool p0)
        {
           // throw new NotImplementedException();
        }

        public void OnPlayerError(ExoPlaybackException p0)
        {
            //throw new NotImplementedException();
        }

        public void OnPlayerStateChanged(bool p0, int p1)
        {
            //throw new NotImplementedException();
            if(p1 == ExoPlayer.StateEnded)
            {
                p.SeekTo(0);
            }
            if (p1 == ExoPlayer.StateReady)
            {
                p.PlayWhenReady = true;
            }
        }

        public void OnPositionDiscontinuity()
        {
           // throw new NotImplementedException();
        }

        public void OnTimelineChanged(Timeline p0, Java.Lang.Object p1)
        {
           // throw new NotImplementedException();
        }

        public void OnTracksChanged(TrackGroupArray p0, TrackSelectionArray p1)
        {
           // throw new NotImplementedException();
        }
    }

    public class TextViewClickListener : Java.Lang.Object, View.IOnClickListener
    {
        Context c;
        string text;
        public TextViewClickListener( Context context,string t)
        {
            c = context;
            text = t;
        }
        public void OnClick(View v)
        {
            string regexp = @"\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))";
            Regex regx = new Regex(regexp, RegexOptions.IgnoreCase);
            MatchCollection mactches = regx.Matches(text);
            IList<string> urls = new List<string>();
            foreach (Match match in mactches)
            {
                urls.Add(match.Value.Replace('"',' ').Replace('>',' ').Replace('<', ' ').Trim());
            }
            if (urls.Count > 0)
            {
                Intent intent = new Intent(c, typeof(TextUrls));
                intent.PutStringArrayListExtra("urls", urls);
                c.StartActivity(intent);
            }
        }
    }
}
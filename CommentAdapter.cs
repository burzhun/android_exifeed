using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Exifeed;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace AndroidGesture
{
    class CommentAdapter : BaseAdapter<Comment>
    {
        Context context;
        List<Comment> list;
        int ScreenWidth;
        Comment comment;
        bool mycompage = false;
        public CommentAdapter(List<Comment> list1, Context context1,bool mycoms=false)
        {
            list = list1;
            context = context1;
            var metrics = context.Resources.DisplayMetrics;
            ScreenWidth = metrics.WidthPixels;
            mycompage = mycoms;
        }
        public override Comment this[int position]
        {
            get
            {
                return list[position];
            }
        }

        public override int Count
        {
            get
            {
                return list.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            comment = list[position];
            if (convertView == null)
            {
                row = LayoutInflater.From(context).Inflate(Resource.Layout.comment, null, false);                         
            }
            if (UserParameters.retrieveset("user_id") != null)
            {
                if (UserParameters.retrieveset("user_id") == comment.parent_comment_user_id && comment.seen == 0)
                {
                    LinearLayout main = row.FindViewById<LinearLayout>(Resource.Id.comment_main_layout);
                    main.SetBackgroundColor(Color.LemonChiffon);
                }
            }
            TextView post_link = row.FindViewById<TextView>(Resource.Id.comment_post_link);
            if (mycompage)
            {
                post_link.Visibility = ViewStates.Visible;
                post_link.Text = comment.post_title;
                post_link.SetOnClickListener(new CommentPostTitleClickListener(comment.post_id, context));
            }else
            {
                post_link.Visibility = ViewStates.Gone;
            }
            RelativeLayout parentRl = row.FindViewById<RelativeLayout>(Resource.Id.parent_comment_layout);
            parentRl.Visibility = ViewStates.Gone;
            if (comment.level != "1")
            {
                parentRl.Visibility = ViewStates.Visible;
                TextView p_username = row.FindViewById<TextView>(Resource.Id.parent_comment_username);
                p_username.Text = comment.parent_comment_username;
                ImageView p_userimage = row.FindViewById<ImageView>(Resource.Id.parent_comment_userimage);
                Koush.UrlImageViewHelper.SetUrlDrawable(p_userimage, comment.parent_comment_userimage);
                RelativeLayout parentComRL = row.FindViewById<RelativeLayout>(Resource.Id.parent_comment_container);
                parentComRL.RemoveAllViews();
                if (comment.parent_comment_text != "")
                {
                    TextView tv = new TextView(context);
                    tv.Text = comment.parent_comment_text;
                    parentComRL.AddView(tv);
                }
                if (comment.parent_comment_image != "")
                {
                    ImageView im = new ImageView(context);
                    im.SetScaleType(ImageView.ScaleType.FitStart);
                    Koush.UrlImageViewHelper.SetUrlDrawable(im, comment.parent_comment_image);
                    parentComRL.AddView(im);
                }
            }
            TextView username = row.FindViewById<TextView>(Resource.Id.comment_username);
            username.Text = comment.username;
            ImageView userimage = row.FindViewById<ImageView>(Resource.Id.comment_image);
            Koush.UrlImageViewHelper.SetUrlDrawable(userimage, comment.userimage);
            LinearLayout commentRL = row.FindViewById<LinearLayout>(Resource.Id.comment_container);
            commentRL.RemoveAllViews();
            if (comment.text != "")
            {
                TextView tv = new TextView(context);
                tv.SetTextColor(Android.Graphics.Color.Rgb(30, 30, 30));
                tv.AutoLinkMask = Android.Text.Util.MatchOptions.WebUrls;
                tv.Text = comment.text;
                tv.InputType = Android.Text.InputTypes.TextFlagMultiLine;
                tv.SetTextSize(Android.Util.ComplexUnitType.Dip, 18);
                commentRL.AddView(tv);
            }
            if (comment.image_url != "")
            {
                ImageView im = new ImageView(context);
                im.SetScaleType(ImageView.ScaleType.FitStart);
                Koush.UrlImageViewHelper.SetUrlDrawable(im, comment.image_url);
                if (comment.video_url == "")
                {
                    im.SetOnClickListener(new CommentImageClickListener(comment.image_url, context));
                    im.SetOnLongClickListener(new PlayButtonLongClickListener(comment.image_url, context,false));
                    commentRL.AddView(im);
                }else
                {
                    RelativeLayout im_layout = new RelativeLayout(context);
                    im_layout.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                    im_layout.AddView(im);
                    im_layout.AddView(CreatePlayButton(context, comment.video_url));
                    commentRL.AddView(im_layout);
                }
                
            }
           // RelativeLayout comment_layout = row.FindViewById<RelativeLayout>(Resource.Id.comment_layout);
           
            return row;
        }

       

        private ImageView CreatePlayButton(Context context,string video)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(80, 80);
            im.LayoutParameters = p;
            p.AddRule(LayoutRules.CenterInParent);
            im.SetAdjustViewBounds(true);
            im.SetImageResource(Resource.Drawable.play_icon);
            im.SetOnLongClickListener(new PlayButtonLongClickListener(video, context));
            im.Click += delegate
            {
                ImageClick(context, video);
            };
            return im;
        }

        private void ImageClick(Context context,string url,bool isImage = false)
        {
            if (isImage)
            {
                var intent1 = new Intent(context, typeof(ImageViewer)).SetFlags(ActivityFlags.ClearTop);
                intent1.PutExtra("image_src", url);
                context.StartActivity(intent1);
            }
            else
            {
                Intent intent = new Intent(context, typeof(Video));
                intent.PutExtra("video", url);
                context.StartActivity(intent);
            }                   
        }
    }

    public class CommentPostTitleClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string post_id;
        Context c;
        public CommentPostTitleClickListener(string Post_id , Context context)
        {
            post_id = Post_id;
            c = context;
        }
        public void OnClick(View v)
        {
            Intent intent = new Intent(c, typeof(PostView)).SetFlags(ActivityFlags.ClearTop);
            intent.PutExtra("post_id", post_id);
            c.StartActivity(intent);
        }
    }

    public class CommentImageClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string url;
        Context c;
        public CommentImageClickListener(string url1, Context context)
        {
            url = url1;
            c = context;
        }
        public void OnClick(View v)
        {
            Intent intent = new Intent(c, typeof(ImageViewer)).SetFlags(ActivityFlags.ClearTop);
            intent.PutExtra("image_src", url);
            c.StartActivity(intent);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    [Activity(Label = "About")]
    public class About : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.About);
            // Create your application here
            LinearLayout l = FindViewById<LinearLayout>(Resource.Id.about_layout);
            EditText about_text = FindViewById<EditText>(Resource.Id.about_input);
            Button about_button = FindViewById<Button>(Resource.Id.about_button);
            TextView about_text2 = FindViewById<TextView>(Resource.Id.about_added);
            about_button.Click += delegate
            {
                if (about_text.Text != "")
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("text", about_text.Text);
                    Console.WriteLine(UrlManager.PostRequest("about", dict));
                    about_text2.Visibility = ViewStates.Visible;
                    l.Visibility = ViewStates.Gone;
                }
            };
        }
    }
}
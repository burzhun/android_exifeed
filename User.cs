using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    class User
    {
        public string id;
        public string username;
        public string image;
        public string shorten_post;
        public string unread_comments;
        public User(string Id,string Username,string Image,string shorten)
        {
            id = Id;
            username = Username;
            image = Image;
            shorten_post = shorten;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ImageViews.Photo;

namespace AndroidGesture
{
    [Activity(Label = "ImageViewer")]
    public class ImageViewer : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.RequestFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.image_viewer);
            // Create your application here
            string image = Intent.GetStringExtra("image_src");
            ImageView im = FindViewById<ImageView>(Resource.Id.image_viewer);
            var attacher = new PhotoViewAttacher(im);
            Koush.UrlImageViewHelper.SetUrlDrawable(im, image);
            attacher.Update();
        }
    }

    
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    [Activity(Label = "CommentAction", Theme = "@android:style/Theme.Translucent.NoTitleBar.Fullscreen")]
    public class CommentAction : Activity
    {
        List<String> actions_list = new List<string> { "Profile", "Respond" };
        string user_id;
        string comment_id;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.comment_action);
            // Create your application here
            if (UserParameters.retrieveset("token") == null)
            {
                actions_list = new List<string> { "Profile" };
            }
            comment_id = Intent.GetStringExtra("comment_id");
            string text = Intent.GetStringExtra("comment_text");
            user_id = Intent.GetStringExtra("user_id");
            ListView listview = FindViewById<ListView>(Resource.Id.comments_actions_list);
            
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,Resource.Layout.comment_action_row,Resource.Id.comments_actions_list_view, actions_list);

            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.comments_actions_list_layout);
            rl.Click += delegate {
                Finish();
            };
            listview.ItemClick += item_click;
            // ����������� ������� ������
            listview.Adapter = adapter;
        }

        private void item_click(object sender, AdapterView.ItemClickEventArgs e)
        {
            string action = actions_list[e.Position];
            if(action == "Profile")
            {
                Intent intent = new Intent(this, typeof(UserActivity));
                intent.PutExtra("user_id", user_id);
                StartActivity(intent);
                Finish();
            }
            if(action == "Respond")
            {
                Intent intent = new Intent(this, typeof(CommentRespond));
                intent.PutExtra("comment_id", comment_id);
                string post_id = Intent.GetStringExtra("post_id");
                intent.PutExtra("post_id", post_id);
                StartActivity(intent);
                Finish();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using System.Net;
using Exifeed;
using Android.Provider;
using Android.Graphics;
using Java.IO;
using Android.Database;
using Android.Text;
using System.Threading.Tasks;

namespace AndroidGesture
{
    [Activity(Label = "Post")]
    public class PostView : Activity
    {
        WebView webView;
        List<Comment> comments_list;
        string post_id;
        ListView list_com;
        string urls;
        string giphy_video, giphy_image;
        CommentAdapter adapter;
        LinearLayout img_preview_layout;
        bool login = false;
        Dictionary<string, string> dict;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.RequestFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.post);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            string token = UserParameters.retrieveset("token");
            post_id = Intent.GetStringExtra("post_id");
            list_com = FindViewById<ListView>(Resource.Id.commentsList);
            RelativeLayout add_comment_layout = FindViewById<RelativeLayout>(Resource.Id.add_comment_layout);
            dict = new Dictionary<string, string>();
            if (token != null)
            {
                dict.Add("token", token);
                add_comment_layout.Visibility = ViewStates.Visible;
                View v = LayoutInflater.From(this).Inflate(Resource.Layout.empty, null, false);
                list_com.AddFooterView(v);
                login = true;
            }
            dict.Add("post_id", post_id);
            var awaiter_post = UrlManager.PostRequestAsync("post", dict).GetAwaiter();
            awaiter_post.OnCompleted(()=>
            {
                if (!download_Completed(awaiter_post.GetResult()))
                {
                    Finish();
                    return;
                }
                var awaiter = UrlManager.PostRequestAsync("comments", dict).GetAwaiter();
                awaiter.OnCompleted(() => {
                    comments_downloaded(awaiter.GetResult());
                });
                ImageView pick_image_button = FindViewById<ImageView>(Resource.Id.comment_image_add);
                pick_image_button.Click += pick_image;
                img_preview_layout = FindViewById<LinearLayout>(Resource.Id.add_comment_image_preview_layout);
                TextView send_button = FindViewById<TextView>(Resource.Id.add_comment_button);
                send_button.Click += add_comment;
                RelativeLayout comments_layout = FindViewById<RelativeLayout>(Resource.Id.post_comments_button_layout);
                comments_layout.Visibility = ViewStates.Gone;
                TextView delete_respond_image = FindViewById<TextView>(Resource.Id.comment_preview_delete);
                delete_respond_image.Click += delegate {
                    LinearLayout rl1 = FindViewById<LinearLayout>(Resource.Id.add_comment_image_preview_layout);
                    rl1.Visibility = ViewStates.Gone;
                    App.bitmap = null;
                    App.video = null;
                    giphy_image = "";
                    giphy_video = "";
                };
            });                   
        }

        private async void comments_downloaded(string json)
        {
            if (json != "error" && json != "")
            {
                if (comments_list == null)
                {
                    comments_list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Comment>>(json);
                    adapter = new CommentAdapter(comments_list, this);
                    list_com.Adapter = adapter;
                    list_com.ItemClick += item_click;
                    if (Intent.HasExtra("to_comments"))
                    {
                        list_com.SmoothScrollToPosition(1);
                    }
                    await Task.Delay(MainActivity.comment_check_interval);
                    update_comments();
                }
                else
                {
                    List<Comment> comments_list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Comment>>(json);
                    comments_list.AddRange(comments_list2);
                    adapter.NotifyDataSetChanged();
                    await Task.Delay(MainActivity.comment_check_interval);
                    update_comments();
                }
            }
        }

        private void add_comment(object sender, EventArgs e)
        {
            string token = UserParameters.retrieveset("token");
            EditText comment_text = FindViewById<EditText>(Resource.Id.add_comment_text);
            if (token == null) return;
            if (comment_text.Text == "" && App.bitmap == null && giphy_image == "" && App.video == null)
            {
                return;
            }
            System.Console.WriteLine(giphy_image);
            Comment.add_comment(token, post_id, "0", comment_text.Text, App.bitmap,App.video, giphy_image,giphy_video);
            VideoView v = FindViewById<VideoView>(Resource.Id.comment_video_preview);
            v.Visibility = ViewStates.Gone;
            ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_image_preview);
            img_preview.Visibility = ViewStates.Gone;
            img_preview_layout.Visibility = ViewStates.Gone;
            // update_comments();
        }

        private void item_click(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (e.Position == 0) return;
            Intent intent = new Intent(this, typeof(CommentAction)).SetFlags(ActivityFlags.ClearTop);
            intent.PutExtra("comment_id", comments_list[e.Position - 1].id);
            intent.PutExtra("post_id", post_id);
            intent.PutExtra("user_id", comments_list[e.Position - 1].user_id);
            StartActivityForResult(intent,ADD_COMMENT);
        }

        public bool download_Completed(string json)
        {
            System.Console.WriteLine("1111111111111111111111111111");
            System.Console.WriteLine(json);
            View row = LayoutInflater.From(this).Inflate(Resource.Layout.post_layout, null, false);
            Post post = null;
            try
            {
                post = Newtonsoft.Json.JsonConvert.DeserializeObject<Post>(json);
                System.Console.WriteLine(json);
            }catch(Exception e1)
            {
                Finish();
                return false;
            }
            if (post == null||json=="")
            {
                return false;

            }
            LinearLayout l = row.FindViewById<LinearLayout>(Resource.Id.postContainer);
            TextView t = row.FindViewById<TextView>(Resource.Id.postTitle);
            TextView description = row.FindViewById<TextView>(Resource.Id.description);
            List<PostPart> list2 = null;
            try
            {
                list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PostPart>>(post.text);
            }
            catch (Exception e1)
            {            }
            ImageView userImage = row.FindViewById<ImageView>(Resource.Id.PostUserImage);
            string userimage = post.userimage.Contains("http") ? post.userimage : "http://www.exifeed.com" + post.userimage;
            Koush.UrlImageViewHelper.SetUrlDrawable(userImage, userimage);
            TextView username = row.FindViewById<TextView>(Resource.Id.PostUsername);
            username.Text = post.username;
            TextView addDate = row.FindViewById<TextView>(Resource.Id.PostDate);
            addDate.Text = post.elapsed_time;
            foreach (PostPart part in list2)
            {
                l.AddView(part.GetLayout(this,0));
                RelativeLayout rl = new RelativeLayout(this);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                rl.LayoutParameters = p;
                l.AddView(rl);
            }
            t.Text = post.title;
            description.Visibility = ViewStates.Gone;
            if (post.description != "")
            {
                description.Visibility = ViewStates.Visible;
                description.Text = post.description;
            }
            list_com.AddHeaderView(row);
            LinearLayout tags_layout = FindViewById<LinearLayout>(Resource.Id.tags_layout);
            tags_layout.RemoveAllViews();
            string[] tags = post.tags.Split(',');
            Color tags_color = new Color(Android.Support.V4.Content.ContextCompat.GetColor(this, Resource.Color.top_background));
            SpannableStringBuilder builder = new SpannableStringBuilder();
            int tag_start = 0;
            tags_layout.Visibility = ViewStates.Gone;
            foreach (string tag in tags)
            {
                if (tag != "")
                {
                    var clickableSpan = new MyClickableSpan(tag, this);
                    builder.Append("#" + tag + "  ");
                    builder.SetSpan(clickableSpan, tag_start, tag_start + tag.Length + 1, Android.Text.SpanTypes.ExclusiveExclusive);
                    tag_start += tag.Length + 3;
                }
            }
            if (tag_start > 0) tags_layout.Visibility = ViewStates.Visible;
            TextView tag_view = new TextView(this);
            tag_view.MovementMethod = new Android.Text.Method.LinkMovementMethod();
            tag_view.Clickable = false;
            tag_view.Append(builder);
            tag_view.SetTextColor(tags_color);
            tag_view.SetTextSize(Android.Util.ComplexUnitType.Dip, 16);
            tags_layout.AddView(tag_view);
            ImageView save_post = FindViewById<ImageView>(Resource.Id.save_post);
            TextView upvote = FindViewById<TextView>(Resource.Id.post_upvote_button);
            username.Click += delegate
            {
                Intent intent = new Intent(this, typeof(UserActivity)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("user_id", post.user_id.ToString());
                StartActivity(intent);
            };
            userImage.Click += delegate
            {
                Intent intent = new Intent(this, typeof(UserActivity)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("user_id", post.user_id.ToString());
                StartActivity(intent);
            };
            if (login)
            {
                upvote.Visibility = ViewStates.Visible;
                if (post.user_rating == 1)
                {
                    String htmlString = "<u>" + post.rating + " Upvoted</u>";
                    upvote.SetTextColor(Color.Rgb(107, 138, 80));
                    upvote.TextFormatted = Html.FromHtml(htmlString);
                    upvote.SetOnClickListener(new UpvoteButtonClickListener(post.id.ToString(), this,  ref upvote, post.rating, true));
                }
                else
                {
                    String htmlString = "<u>" + post.rating + " Upvote</u>";
                    upvote.SetTextColor(Color.Rgb(10, 10, 10));
                    upvote.TextFormatted = Html.FromHtml(htmlString);
                    upvote.SetOnClickListener(new UpvoteButtonClickListener(post.id.ToString(), this,  ref upvote, post.rating));
                }
                save_post.Visibility = ViewStates.Visible;
                if (post.saved == 1)
                {
                    save_post.Alpha = 1;
                    save_post.SetOnClickListener(new SaveButtonClickListener(post.id.ToString(), this,  ref save_post));
                }
                else
                {
                    save_post.SetOnClickListener(new SaveButtonClickListener(post.id.ToString(), this, ref save_post, true));
                    save_post.Alpha = 0.4f;
                }
            }
            else
            {
                save_post.Visibility = ViewStates.Gone;
                upvote.Visibility = ViewStates.Gone;
            }
            return true;
        }

        private void pick_image(object sender, EventArgs e)
        {
            String[] items = { "Take Photo", "Choose from Library", "Load from Giphy or Imgur","Popular images","Upload video", "Cancel" };
            using (var dialogBuilder = new AlertDialog.Builder(this))
            {
                dialogBuilder.SetTitle("Add Image");
                dialogBuilder.SetItems(items, (d, args) => {
                    //Take photo
                    if (args.Which == 0)
                    {
                        CreateDirectoryForPictures();
                        var intent = new Intent(MediaStore.ActionImageCapture);
                        App._file = new Java.IO.File(App._dir, string.Format("karma_{0}.jpg", Guid.NewGuid()));
                        intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
                        this.StartActivityForResult(intent, REQUEST_CAMERA);
                    }
                    //Choose from gallery
                    else if (args.Which == 1)
                    {

                        var intent = new Intent(Intent.ActionPick, MediaStore.Images.Media.ExternalContentUri);
                        intent.SetType("image/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), SELECT_FILE);
                    }else if(args.Which == 2)
                    {
                        var intent = new Intent(this, typeof(GiphySearch));
                        this.StartActivityForResult(intent, GIPHY_SEARCH);
                    }
                    else if (args.Which == 3)
                    {
                        var intent = new Intent(this, typeof(UserImages));
                        this.StartActivityForResult(intent, GIPHY_SEARCH);
                    }
                    else if(args.Which == 4)
                    {
                        var intent = new Intent(Intent.ActionPick, MediaStore.Video.Media.ExternalContentUri);
                        intent.SetType("video/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Video"), SELECT_VIDEO);
                    }
                });

                dialogBuilder.Show();
            }
        }

        public static class App
        {
            public static Java.IO.File _file;
            public static Java.IO.File _dir;
            public static Bitmap bitmap;
            public static string video;  
        }

        public void update_comments()
        {
            if (dict.ContainsKey("offset"))
            {
                dict["offset"] = comments_list.Count.ToString();
            }
            else
            {
                dict.Add("offset", comments_list.Count.ToString());
            }
            var awaiter = UrlManager.PostRequestAsync("comments", dict).GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                comments_downloaded(awaiter.GetResult());
            });
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (resultCode == Result.Ok)
            {                              
                
                if (requestCode == REQUEST_CAMERA)
                {
                    // Handle the newly captured image
                    Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                    Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
                    mediaScanIntent.SetData(contentUri);
                    SendBroadcast(mediaScanIntent);

                    // Display in ImageView. We will resize the bitmap to fit the display.
                    // Loading the full sized image will consume to much memory
                    // and cause the application to crash.

                    //int height = Resources.DisplayMetrics.HeightPixels;
                    VideoView v = FindViewById<VideoView>(Resource.Id.comment_video_preview);
                    v.Visibility = ViewStates.Gone;
                    ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_image_preview);
                    img_preview.Visibility = ViewStates.Visible;
                    int width = 720;
                    App.bitmap = App._file.Path.LoadAndResizeBitmap(width);
                    if (App.bitmap != null)
                    {
                        img_preview_layout.Visibility = ViewStates.Visible;
                        int h = (int)App.bitmap.Height * 80 / App.bitmap.Width;
                        img_preview.SetImageBitmap(Bitmap.CreateScaledBitmap(App.bitmap, 80, h, false));
                        App.bitmap = null;
                        giphy_image = "";
                        giphy_video = "";
                        App.bitmap = null;
                    }

                    // Dispose of the Java side bitmap.
                    GC.Collect();
                }
                else if (requestCode == SELECT_FILE)
                {
                    // Handle the image chosen from gallery
                    VideoView v = FindViewById<VideoView>(Resource.Id.comment_video_preview);
                    v.Visibility = ViewStates.Gone;
                    ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_image_preview);
                    img_preview.Visibility = ViewStates.Visible;
                    Android.Net.Uri uri = data.Data;
                    img_preview_layout.Visibility = ViewStates.Visible;
                    App.bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, uri);
                    img_preview.SetImageURI(uri);
                    giphy_image = "";
                    giphy_video = "";
                    App.video = null;
                }
                else if(requestCode == GIPHY_SEARCH)
                {
                    if (data.HasExtra("image"))
                    {
                        App.bitmap = null;
                        App.video = null;
                        giphy_video = data.GetStringExtra("video");
                        giphy_image = data.GetStringExtra("image");
                        VideoView v = FindViewById<VideoView>(Resource.Id.comment_video_preview);
                        v.Visibility = ViewStates.Gone;
                        ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_image_preview);
                        img_preview.Visibility = ViewStates.Visible;
                        img_preview_layout.Visibility = ViewStates.Visible;
                        Koush.UrlImageViewHelper.SetUrlDrawable(img_preview, giphy_image);
                    }                    
                }
                else if(requestCode == SELECT_VIDEO)
                {
                    Android.Net.Uri uri = data.Data;
                    img_preview_layout.Visibility = ViewStates.Visible;
                    VideoView v = FindViewById<VideoView>(Resource.Id.comment_video_preview);
                    v.Visibility = ViewStates.Visible;
                    ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_image_preview);
                    img_preview.Visibility = ViewStates.Gone;
                    v.SetVideoURI(uri);
                    v.SeekTo(100);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    FileInputStream fis;

                    try
                    {

                        File file = new File(GetRealPathFromURI(this, uri));
                        System.Console.WriteLine(file.Length().ToString());
                        fis = new FileInputStream(file);

                        byte[] buf = new byte[1024];
                        int n;
                        while (-1 != (n = fis.Read(buf)))
                            baos.Write(buf, 0, n);
                    }catch(Exception e)
                    {

                    }                    
                    byte[] bbytes = baos.ToByteArray();
                    App.video = Convert.ToBase64String(bbytes);
                    App.bitmap = null;
                    giphy_image = "";
                }
            }


        }

        public static string GetRealPathFromURI(Context context, Android.Net.Uri uri)
        {

            bool isKitKat = Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat;

            // DocumentProvider
            if (isKitKat && Android.Provider.DocumentsContract.IsDocumentUri(context, uri))
            {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri))
                {
                    string docId = Android.Provider.DocumentsContract.GetDocumentId(uri);
                    string[] split = docId.Split(':');
                    string type = split[0];

                    if ("primary".Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri))
                {

                    string id = Android.Provider.DocumentsContract.GetDocumentId(uri);
                    Android.Net.Uri contentUri = ContentUris.WithAppendedId(Android.Net.Uri.Parse("content://downloads/public_downloads"), Convert.ToInt64(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri))
                {
                    string docId = Android.Provider.DocumentsContract.GetDocumentId(uri);
                    string[] split = docId.Split(':');
                    string type = split[0];

                    Android.Net.Uri contentUri = null;
                    if ("image".Equals(type))
                    {
                        contentUri = Android.Provider.MediaStore.Images.Media.ExternalContentUri;
                    }
                    else if ("video".Equals(type))
                    {
                        contentUri = Android.Provider.MediaStore.Video.Media.ExternalContentUri;
                    }
                    else if ("audio".Equals(type))
                    {
                        contentUri = Android.Provider.MediaStore.Audio.Media.ExternalContentUri;
                    }

                    string selection = "_id=?";
                    string[] selectionArgs = new string[] {
                    split[1]
            };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return uri.Path;
            }

            return null;
        }

        /**
         * Get the value of the data column for this Uri. This is useful for
         * MediaStore Uris, and other file-based ContentProviders.
         *
         * @param context The context.
         * @param uri The Uri to query.
         * @param selection (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        public static String getDataColumn(Context context, Android.Net.Uri uri, String selection,
                String[] selectionArgs)
        {

            Android.Database.ICursor cursor = null;
            string column = "_data";
            string[] projection = {
                column
            };

            try
            {
                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs,
                        null);
                if (cursor != null && cursor.MoveToFirst())
                {
                    int column_index = cursor.GetColumnIndexOrThrow(column);
                    return cursor.GetString(column_index);
                }
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }
            return null;
        }


        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        public static bool isExternalStorageDocument(Android.Net.Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        public static bool isDownloadsDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        public static bool isMediaDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        private static readonly Int32 REQUEST_CAMERA = 0;
        private static readonly Int32 SELECT_FILE = 1;
        private static readonly Int32 ADD_COMMENT = 2;
        private static readonly Int32 GIPHY_SEARCH = 3;
        private static readonly Int32 SELECT_VIDEO = 4;


        public static void CreateDirectoryForPictures()
        {
            App._dir = new Java.IO.File(
                Android.OS.Environment.GetExternalStoragePublicDirectory(
                    Android.OS.Environment.DirectoryPictures), "CameraAppDemo");
            if (!App._dir.Exists())
            {
                App._dir.Mkdirs();
            }
        }
    }
}
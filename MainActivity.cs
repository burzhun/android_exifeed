﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exifeed;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Util;
using System.Net;
using com.refractored;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Graphics.Drawables;
using Android.Support.V4.View;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Threading;

namespace AndroidGesture
{
    [Activity(Label = "MainActivity", MainLauncher = true, Theme = "@style/AppTheme")]
    public class MainActivity : BaseActivity, IOnTabReselectedListener
    {

        private Toolbar toolbar;
        private RecyclerView recyclerView;
        private DrawerAdapter left_adapter;
        private RecyclerView.LayoutManager layoutManager;
        private DrawerLayout drawer;
        private Android.Support.V7.App.ActionBarDrawerToggle drawerToggle;
        TextView txtActionBarText;
        TextView txtDescription;
        EditText search;
        ImageView search_icon;
        private List<Post> list;
        WebClient webClient;
        Uri url;
        ListView listView;
        int offset;
        int limit;
        string PostsType;
        string token;
        string api_key;
        int resume = 0;
        Post_adapter adapter = null;
        string prev_type;
        ImageView search_close_icon;
        string[] Titles;
        private MyPagerAdapter pager_adapter;
        private Drawable oldBackground = null;
        private int currentColor;
        private ViewPager pager;
        private PagerSlidingTabStrip tabs;
        public RelativeLayout seacrh_post_list_layout;
        bool app_started = false;
        public static int comment_check_interval = 8000;
        static System.ComponentModel.BackgroundWorker comments_worker = null;
        int previous_comments_count = 0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            if(UserParameters.retrieveset("app_token") == null)
            {
                UserParameters.saveset("app_token", Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 12));
            }
            var awaiter = UrlManager.PostRequestAsync("all_post", null).GetAwaiter();
            awaiter.OnCompleted(() => {
                pager_adapter = new MyPagerAdapter(SupportFragmentManager, this, awaiter.GetResult());
                pager = FindViewById<ViewPager>(Resource.Id.pager);
                tabs = FindViewById<PagerSlidingTabStrip>(Resource.Id.tabs);
                pager.Adapter = pager_adapter;
                tabs.SetViewPager(pager);

                var pageMargin = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 4, Resources.DisplayMetrics);
                pager.PageMargin = pageMargin;
                tabs.OnTabReselectedListener = this;               
                ChangeColor(Resources.GetColor(Resource.Color.top_background));
                // pager.AddOnPageChangeListener(new MyPagerListeneter());
                FindViewById<RelativeLayout>(Resource.Id.loadingPanel).Visibility = ViewStates.Gone;
            });

            seacrh_post_list_layout = FindViewById<RelativeLayout>(Resource.Id.post_search_list_layout);
            listView = FindViewById<ListView>(Resource.Id.postListViewSearch);
            listView.Scroll += listviewscrolled;
            webClient = new WebClient();
            offset = 0;
            limit = 10;
            PostsType = "top";
            api_key = "sdsgre345345hkb5kh345kj345";
            string url_user = UrlManager.domain + "?api_key=" + api_key;
            token = UserParameters.retrieveset("token");
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (token != null)
            {
                url_user += "&check_token535345=" + token;
                Uri userToken = new Uri(url_user);
                webClient.DownloadDataAsync(userToken);
                webClient.DownloadDataCompleted += check_user;
            }else
            {
                CreateMenu(false, true);
            }
            txtActionBarText = FindViewById<TextView>(Resource.Id.txtActionBarText);
            search = FindViewById<EditText>(Resource.Id.search_field);
            search_icon = FindViewById<ImageView>(Resource.Id.search_icon);
            search_close_icon = FindViewById<ImageView>(Resource.Id.search_close_icon);
            search_icon.Click += delegate {
                search_icon.Visibility = ViewStates.Gone;
                search.Visibility = ViewStates.Visible;
                txtActionBarText.Visibility = ViewStates.Gone;
                search_close_icon.Visibility = ViewStates.Visible;
                seacrh_post_list_layout.Visibility = ViewStates.Visible;
            };
            toolbar.Click += delegate
            {
                if (search.Text == "")
                {
                    close_search();
                }
            };
            search_close_icon.Click += delegate
            {
                close_search();
            };
            search.KeyPress += (o, e) =>
            {
                if (e.KeyCode == Keycode.Enter && e.Event.Action == KeyEventActions.Down)
                {
                    if (search.Text != "")
                    {
                        FindPosts();
                    }
                    e.Handled = true;
                }else
                {
                    e.Handled = false;
                }
            };
            app_started = true;
            
        }

        private void close_search()
        {
            search_icon.Visibility = ViewStates.Visible;
            search.Visibility = ViewStates.Gone;
            txtActionBarText.Visibility = ViewStates.Visible;
            search_close_icon.Visibility = ViewStates.Gone;
            PostsType = prev_type;
            offset = 0;
            seacrh_post_list_layout.Visibility = ViewStates.Gone;
        }

        private void listviewscrolled(object sender, AbsListView.ScrollEventArgs e)
        {
            if (e.View.LastVisiblePosition + 1 == offset + limit)
            {
                FindPosts( offset + limit);
                offset += limit;
            }
        }
        private void check_comments()
        {
            if (UserParameters.retrieveset("token") == null) return;
            if (comments_worker == null)
            {
                comments_worker = new System.ComponentModel.BackgroundWorker();
                comments_worker.DoWork += delegate
                {
                    string json = UrlManager.PostRequest("new_comments_count", null);

                    var jobject = Newtonsoft.Json.Linq.JObject.Parse(json);
                    int count = jobject["result"].ToObject<int>();

                    if (count != previous_comments_count)
                    {
                        previous_comments_count = count;
                        RunOnUiThread(() =>
                        {
                            if (count > 0)
                            {
                                left_adapter.navTitles[2] = "My comments (" + count.ToString() + ")";
                                txtActionBarText.Text = "Exifeed*";
                            }
                            else
                            {
                                left_adapter.navTitles[2] = "My comments";
                                txtActionBarText.Text = "Exifeed";
                            }
                            left_adapter.NotifyDataSetChanged();
                        });
                    }
                };
                comments_worker.RunWorkerCompleted += async delegate
                {
                    await Task.Delay(comment_check_interval);
                    check_comments();
                };
            }
            comments_worker.RunWorkerAsync();
            
        }

        public override void OnBackPressed()
        {
          //  base.OnBackPressed();
            Console.WriteLine("back");
            if (app_started)
            {
                if (seacrh_post_list_layout.Visibility == ViewStates.Visible)
                {
                    close_search();
                }
                else
                {
                    Finish();
                }
            }
            
        }
        protected override void OnResume()
        {
            base.OnResume();
            if (resume == 0)
            {
                resume++;
                return;
            }
            if (token == null && UserParameters.retrieveset("token") != null)
            {
                string url_user = UrlManager.domain + "?api_key=" + UrlManager.api_key;
                // UserParameters.saveset("token", null);
                token = UserParameters.retrieveset("token");
                url_user += "&check_token535345=" + token;
                Uri userToken = new Uri(url_user);
                webClient.DownloadDataAsync(userToken);
                webClient.DownloadDataCompleted += check_user;
                offset = 0;
            }
            if (UserParameters.retrieveset("userimage_changed") != null)
            {
                CreateMenu(true, true);
            }

        }
        public void check_user(object sender, DownloadDataCompletedEventArgs e)
        {
            RunOnUiThread(() =>
            {
                string json = Encoding.UTF8.GetString(e.Result);
                if (json == "nope")
                {
                    token = null;
                    UserParameters.saveset("token", null);
                    CreateMenu(false, true);
                }
                else
                {
                    User user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(json);
                    UserParameters.saveset("username", user.username);
                    UserParameters.saveset("shorten_post", user.shorten_post);
                    UserParameters.saveset("userimage", user.image);
                    UserParameters.saveset("user_id", user.id);
                    UserParameters.saveset("unread_comments", user.unread_comments);
                    CreateMenu(true, true);
                    Task task1 = Task.Run(() =>
                    {
                        Thread.Sleep(comment_check_interval);
                        previous_comments_count = Convert.ToInt32(user.unread_comments);
                        check_comments();
                    });
                }                
            });
        }
        private void FindPosts( int offset1 = 0, int limit1 = 10)
        {
            WebClient webClient = new WebClient();
            NameValueCollection coll = new NameValueCollection();            
            if (offset1 != 0)
            {
                coll["offset"]= offset1.ToString();
            }
            if (limit1 != 10)
            {
                coll["limit"]= limit1.ToString();
            }            
            coll["search"] = search.Text;
            coll["type"] = "search";
            if (UserParameters.retrieveset("token") != null)
            {
                coll["token"] = UserParameters.retrieveset("token");
            }
            string url = UrlManager.domain + "posts" + "?api_key=" + api_key;
            try
            {
                webClient.UploadValuesAsync(new Uri(url), "POST", coll);
                webClient.UploadValuesCompleted += (o,e)=> {
                    string json = Encoding.UTF8.GetString(e.Result);
                    Console.WriteLine(json);
                    if (json != "error" && json != "")
                    {
                        listView.Visibility = ViewStates.Visible;
                        if (offset1 == 0)
                        {
                            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                            adapter = new Post_adapter(list, this);
                            listView.Adapter = adapter;
                        }
                        else
                        {
                            List<Post> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                            list.AddRange(list2);
                            //adapter = new Post_adapter(list, this);
                            adapter.NotifyDataSetChanged();
                            //listView.Adapter = adapter;
                        }
                    }
                    else if (json == "error")
                    {
                        txtActionBarText.Text = "No posts found";
                        listView.Visibility = ViewStates.Invisible;
                    }
                };
               
            }
            catch (Exception e1)
            {
            }
            

        }

        private void WebClient_UploadValuesCompleted(object sender, UploadValuesCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void LeftMenuClick(string text)
        {
            Console.WriteLine(text);
            drawer.CloseDrawer(recyclerView);
            if (text == "Login")
            {
                Intent intent = new Intent(this, typeof(Login));
                StartActivity(intent);
                return;
            }
            if (text == "Logout")
            {
                token = null;
                UserParameters.saveset("username", null);
                UserParameters.saveset("userimage", null);
                CreateMenu(false, true);
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("logout", "1");
                UrlManager.PostRequest("", dict);
                UserParameters.saveset("token", null);
                offset = 0;
                return;
            }
            if (text == "Add post")
            {
                Intent intent = new Intent(this, typeof(AddPost));
                StartActivity(intent);
                return;
            }
            if (text.StartsWith("My comments"))
            {
                Intent intent = new Intent(this, typeof(MyComments));
                StartActivity(intent);
                return;
            }
            if (text.StartsWith("Last comments"))
            {
                Intent intent = new Intent(this, typeof(LastComments));
                StartActivity(intent);
                return;
            }
            if (text == "Settings")
            {
                Intent intent = new Intent(this, typeof(SettingsActivity));
                StartActivity(intent);
            }
            if (text == "About")
            {
                Intent intent = new Intent(this, typeof(About));
                StartActivity(intent);
                return;
            }
           if(text == "Saved posts")
            {
                Intent intent = new Intent(this, typeof(SavedPosts));
                StartActivity(intent);
                return;
            }
        }
        public void CreateMenu(bool logged, bool update = false)
        {
            if (!logged)
            {
                Titles = new string[] {  "Login", "About" };
            }
            else
            {
                Titles = new string[] { "Add post", "Saved posts", "My comments","Last comments", "Settings", "Logout", "About" };
                if(UserParameters.retrieveset("unread_comments")!=null && UserParameters.retrieveset("unread_comments") != "0")
                {
                    Titles[2] += " (" + UserParameters.retrieveset("unread_comments") + ")";
                    txtActionBarText.Text = "Exifeed*";
                }
            }
            SetSupportActionBar(toolbar);
            recyclerView = FindViewById<RecyclerView>(Resource.Id.RecyclerView);
            recyclerView.HasFixedSize = true;

            //Make sure that we always have the minimum right margin of 56dp on the navigation drawer
            var minDrawerMargin = (int)Resources.GetDimension(Resource.Dimension.navigation_drawer_min_margin);
            var maxDrawerWidth = Resources.DisplayMetrics.WidthPixels - minDrawerMargin;
            recyclerView.LayoutParameters.Width = Math.Min(recyclerView.LayoutParameters.Width, maxDrawerWidth);

            left_adapter = new DrawerAdapter(Titles, this);
            recyclerView.SetAdapter(left_adapter);

            layoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);

            drawer = FindViewById<DrawerLayout>(Resource.Id.DrawerLayout);
            drawerToggle = new DrawerToggle(this, drawer, toolbar, Resource.String.open_drawer, Resource.String.close_drawer);
            drawer.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();
            recyclerView.BringToFront();
            drawer.RequestLayout();
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var id = item.ItemId;

            //   if (id == Resource.Id.action_settings)
            //   return true;
            return base.OnOptionsItemSelected(item);
        }        

        public void openMyPage()
        {
            Intent intent = new Intent(this, typeof(UserActivity));
            intent.PutExtra("user_id", UserParameters.retrieveset("user_id"));
            StartActivity(intent);
        }

        protected override int LayoutResource
        {
            get
            {
                return Resource.Layout.Main;
            }
        }
        
        public void OnTabReselected(int position)
        {
            //			Toast.MakeText(this, "Tab reselected: " + position, ToastLength.Short).Show();
        }

        private void ChangeColor(Color newColor)
        {
            if (tabs != null)
            {
                tabs.SetBackgroundColor(newColor);
            }
            // change ActionBar color just if an ActionBar is available
            Drawable colorDrawable = new ColorDrawable(newColor);
            Drawable bottomDrawable = new ColorDrawable(Resources.GetColor(Android.Resource.Color.Transparent));
            LayerDrawable ld = new LayerDrawable(new Drawable[] { colorDrawable, bottomDrawable });
            if (oldBackground == null)
            {
                SupportActionBar.SetBackgroundDrawable(ld);
            }
            else
            {
                TransitionDrawable td = new TransitionDrawable(new Drawable[] { oldBackground, ld });
                SupportActionBar.SetBackgroundDrawable(td);
                td.StartTransition(200);
            }

            oldBackground = ld;
            currentColor = newColor;
        }
        [Java.Interop.Export("onColorClicked")]
        public void OnColorClicked(View v)
        {
            var color = Color.ParseColor(v.Tag.ToString());
            ChangeColor(color);
            Console.WriteLine("4234234");

        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutInt("currentColor", currentColor);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            base.OnRestoreInstanceState(savedInstanceState);
            currentColor = savedInstanceState.GetInt("currentColor");
            ChangeColor(new Color(currentColor));
        }
    }

    internal class MyPagerListeneter : Java.Lang.Object, ViewPager.IOnPageChangeListener
    {
        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
            // throw new NotImplementedException();
            Console.WriteLine(position.ToString() + "  " + positionOffset.ToString());
        }

        public void OnPageScrollStateChanged(int state)
        {
          //  throw new NotImplementedException();
        }

        public void OnPageSelected(int position)
        {
            PostPart.last_played_index = -1;
        }
    }

    public class MyPagerAdapter : FragmentPagerAdapter
    {
        private string[] Titles = {"Top","New","Best","Feed" };
        public Context c;
        public string first_load;
        public MyPagerAdapter(Android.Support.V4.App.FragmentManager fm,Context c1,string s) : base(fm)
        {
            c = c1;
            first_load = s;
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(Titles[position]);
        }
        #region implemented abstract members of PagerAdapter
        public override int Count
        {
            get
            {
                return Titles.Length;
            }
        }
        #endregion
        #region implemented abstract members of FragmentPagerAdapter
        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            return CardFragment.NewInstance(position, c,first_load);
        }
        #endregion
    }
}
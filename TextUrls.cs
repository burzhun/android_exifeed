using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    [Activity(Label = "TextUrls", Theme = "@android:style/Theme.Translucent.NoTitleBar.Fullscreen")]
    public class TextUrls : Activity
    {
        string[] urls;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.texturls);
            urls = Intent.GetStringArrayListExtra("urls").ToArray<string>();
            if (urls.Length == 0) Finish();
            ListView listview = FindViewById<ListView>(Resource.Id.text_urls_list);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, Resource.Layout.urls_row, Resource.Id.urls_row_layout, urls);
            
            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.text_urls_layout);
            rl.Click += delegate {
                Finish();
            };

            listview.ItemClick += item_click;
            // ����������� ������� ������
            listview.Adapter = adapter;
        }

        private void item_click(object sender, AdapterView.ItemClickEventArgs e)
        {
            StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse(urls[e.Position])));
        }
    }
}
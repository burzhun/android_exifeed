using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Text.RegularExpressions;

namespace AndroidGesture
{
    [Activity(Label = "AddLink")]
    public class AddLink : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_link);
            // Create your application here
            EditText url = FindViewById<EditText>(Resource.Id.add_post_link_url);
            EditText title = FindViewById<EditText>(Resource.Id.add_post_link_title);
            TextView tview = FindViewById<TextView>(Resource.Id.add_link_empty);
            Button button = FindViewById<Button>(Resource.Id.add_link_to_post_button);

            button.Click += delegate
            {
                tview.Visibility = ViewStates.Gone;
                if(url.Text == "")
                {
                    tview.Text = "Your link is empty";
                    tview.Visibility = ViewStates.Visible;
                    return;
                }
                string regexp = @"\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))";
                Regex regx = new Regex(regexp, RegexOptions.IgnoreCase);
                if (!regx.IsMatch(url.Text))
                {
                    tview.Text = "Your link is not correct";
                    tview.Visibility = ViewStates.Visible;
                    return;
                }else
                {
                    Intent intent = new Intent();
                    intent.PutExtra("url", url.Text);
                    if (title.Text != "")
                    {
                        intent.PutExtra("title", title.Text);
                    }
                    ((Activity)this).SetResult(Result.Ok, intent);
                    ((Activity)this).Finish();
                }
            };
        }
    }
}
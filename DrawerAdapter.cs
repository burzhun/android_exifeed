using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;
using AndroidGesture;
using Android.Graphics.Drawables;

namespace Exifeed
{

	public class DrawerAdapter : RecyclerView.Adapter
	{
		public enum ViewType
		{
			Header = 0,
			Item = 1
		}
        Context mainContext;
		public string[] navTitles;
		int[] navIcons;

		public DrawerAdapter(string[] titles, Context mainContext1)
		{
			navTitles = titles;
            mainContext = mainContext1;
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			if (viewType == (int)ViewType.Header) {
				var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.DrawerHeader, parent, false);
				return new DrawerViewHolder(view, ViewType.Header);
			}
			else if (viewType == (int)ViewType.Item) {
				var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.DrawerItemRow, parent, false);
                
				return new DrawerViewHolder(view, ViewType.Item);
			}

			return null;
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			var viewHolder = holder as DrawerViewHolder;

			if (viewHolder.HolderType == ViewType.Item) {
				viewHolder.TextView.Text = navTitles[position - 1];
                //viewHolder.ImageView.SetImageResource(navIcons[position - 1]);
                viewHolder.TextView.SetOnClickListener(new LeftMenuClickListener(navTitles[position - 1], mainContext));

            }
			else {
                if(UserParameters.retrieveset("username") != null)
                {
                    viewHolder.Name.Text = UserParameters.retrieveset("username");
                }
                else
                {
                    viewHolder.Name.Text = "";
                }
                if (UserParameters.retrieveset("userimage") != null)
                {
                    if(UserParameters.retrieveset("userimage") != "http://exifeed.com/images/icons/no_image_user.png")
                    {
                        Koush.UrlImageViewHelper.SetUrlDrawable(viewHolder.profile, UserParameters.retrieveset("userimage"));
                        viewHolder.profile.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        viewHolder.profile.Visibility = ViewStates.Gone;
                    }
                }
                
                viewHolder.Name.Click += delegate
                {
                    ((MainActivity)mainContext).openMyPage();
                };
                viewHolder.profile.Click += delegate
                {
                    ((MainActivity)mainContext).openMyPage();
                };
                /*
				holder.profile.setImageResource(profile);           // Similarly we set the resources for header view
				holder.Name.setText(name);
				holder.email.setText(email);*/

            }
		}

		public override int ItemCount {
			get {
				return navTitles.Length + 1;
			}
		}

		public override int GetItemViewType(int position)
		{
			return IsPositionHeader(position) ? (int)ViewType.Header : (int)ViewType.Item;

		}

		private static bool IsPositionHeader(int position)
		{
			return position == 0;
		}

        public class LeftMenuClickListener : Java.Lang.Object, View.IOnClickListener
        {
            string text;
            Context c;
            public LeftMenuClickListener(string Text, Context context)
            {
                text = Text;
                c = context;
            }
            public void OnClick(View v)
            {
                    ((MainActivity)c).LeftMenuClick(text);
            }
        }

        public class DrawerViewHolder : RecyclerView.ViewHolder
		{
			public ViewType HolderType { get; private set; }

			public TextView TextView { get; set; }
            public TextView Name, email;
            public ImageView profile;
            public RelativeLayout layout;
            public ImageView ImageView { get; private set; }

			public DrawerViewHolder(View itemView, ViewType viewType) : base(itemView)
			{
				HolderType = viewType;

				if (viewType == ViewType.Item) {
					TextView = itemView.FindViewById<TextView>(Resource.Id.rowText);
					//ImageView = itemView.FindViewById<ImageView>(Resource.Id.rowIcon);
				}
				else {
                    int[] backgrounds = { Resource.Drawable.app_background1, Resource.Drawable.app_background2, Resource.Drawable.app_background3, Resource.Drawable.app_background4, Resource.Drawable.app_background5 };
                    layout = itemView.FindViewById<RelativeLayout>(Resource.Id.user_layout);
                    Random random = new Random();
                    int randomNumber = random.Next(0, 4);
                    layout.SetBackgroundResource(backgrounds[randomNumber]);
					Name = (TextView) itemView.FindViewById(Resource.Id.name);         // Creating Text View object from header.xml for name
					//email = (TextView) itemView.FindViewById(Resource.Id.email);       // Creating Text View object from header.xml for email
					profile = (ImageView) itemView.FindViewById(Resource.Id.user_layout_image);// Creating Image view object from header.xml for profile pic	
                                       
            }

			}
		}

        


    }

}

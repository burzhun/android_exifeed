using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Provider;
using Android.Graphics;
using Android.Text;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using Java.IO;

namespace AndroidGesture
{
    [Activity(Label = "AddPost")]    
    public class AddPost : Activity
    {
        LinearLayout list_layout;
        List<PostPart2> post_parts = new List<PostPart2>();
        EditText title;
        EditText description;
        bool title_pressed = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_post);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            // Create your application here
            list_layout = FindViewById<LinearLayout>(Resource.Id.add_post_list_layout);
            title = FindViewById<EditText>(Resource.Id.add_post_title);
            description = FindViewById<EditText>(Resource.Id.add_post_description);
            Button upload = FindViewById<Button>(Resource.Id.add_post_button);
            ImageButton add_part = FindViewById<ImageButton>(Resource.Id.add_post_part);
            add_part.Click += add_to_post;
            upload.Click += upload_post;
            title.KeyPress += title_key;
            description.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    if (title_pressed)
                    {
                        title_pressed = false;
                        return;
                    }
                    description.ClearFocus();
                    add_to_post(null,null);
                }
                else
                {
                    e.Handled = false;
                }
            };
        }

        private void title_key(object sender, View.KeyEventArgs e)
        {
            if (e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                title_pressed = true;
                title.ClearFocus();
                description.RequestFocus();
               
            }else
            {
                e.Handled = false;
            }
        }

        private void upload_post(object sender, EventArgs e)
        {
            if(title.Text == "")
            {
                ShowMessage("You need to app post title");
                return;
            }
            if(post_parts.Count == 0)
            {
                ShowMessage("Your post is empty");
                return;
            }
            string post=Newtonsoft.Json.JsonConvert.SerializeObject(post_parts).ToString();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("post", post);
            dict.Add("title", title.Text);
            if(description.Text != "")
            {
                dict.Add("description", description.Text);
            }
            string response = "";
            try
            {
                response = UrlManager.PostRequest("addpost", dict);
            }catch(Exception e2)
            {
                response = "error";
            }
            if (response == "post_uploaded")
            {
                Finish();
            }else
            {
                ShowMessage("There has been some error.Sorry :(");
                System.Console.WriteLine(response);
            }
        }

        private int AddTextCode = 10;
        private int AddVideoCode = 11;
        private void add_to_post(object sender, EventArgs e)
        {
            String[] items = { "Add text","Add image","Upload video",  "Add video from Youtube,Coub","Add link", "Cancel" };

            using (var dialogBuilder = new AlertDialog.Builder(this))
            {
                dialogBuilder.SetTitle("Add Photo");
                dialogBuilder.SetItems(items, (d, args) => {
                    if (args.Which == 0)
                    {
                        var intent = new Intent(this, typeof(AddText));
                        StartActivityForResult(intent, AddTextCode);
                    }
                   
                    else if (args.Which == 1)
                    {
                        add_imageto_post();
                    }
                    else if(args.Which == 2)
                    {
                        var intent = new Intent(Intent.ActionPick, MediaStore.Video.Media.ExternalContentUri);
                        intent.SetType("video/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Video"), SELECT_VIDEO);
                    }
                    else if (args.Which == 3)
                    {
                        var intent = new Intent(this, typeof(AddVideo));
                        StartActivityForResult(intent, AddVideoCode);
                    }else if(args.Which == 4)
                    {
                        var intent = new Intent(this, typeof(AddLink));
                        StartActivityForResult(intent, ADD_LINK);
                    }
                });

                dialogBuilder.Show();
            }
        }
        public static class App
        {
            public static Java.IO.File _file;
            public static Java.IO.File _dir;
            public static Bitmap bitmap;
        }

        public class PostPart2
        {
            public string type;
            public string content;
            public string video;            
            public PostPart2(string type1, string image1, string video1= "")
            {
                type = type1;
                content = image1;
                video = video1;
            }
        }
        private static readonly Int32 REQUEST_CAMERA = 0;
        private static readonly Int32 SELECT_FILE = 1;
        private static readonly Int32 GIPHY_SEARCH = 2;
        private static readonly Int32 SELECT_VIDEO = 3;
        private static readonly Int32 ADD_LINK = 4;
        private void add_imageto_post()
        {
            String[] items = { "Take Photo", "Choose from Library", "Load from Giphy or Imgur", "Cancel" };

            using (var dialogBuilder = new AlertDialog.Builder(this))
            {
                dialogBuilder.SetTitle("Add Photo");
                dialogBuilder.SetItems(items, (d, args) => {
                    //Take photo
                    if (args.Which == 0)
                    {
                        PostView.CreateDirectoryForPictures();
                        var intent = new Intent(MediaStore.ActionImageCapture);
                        App._file = new Java.IO.File(App._dir, string.Format("karma_{0}.jpg", Guid.NewGuid()));
                        intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
                        this.StartActivityForResult(intent, REQUEST_CAMERA);
                    }
                    //Choose from gallery
                    else if (args.Which == 1)
                    {
                        var intent = new Intent(Intent.ActionPick, MediaStore.Images.Media.ExternalContentUri);
                        intent.SetType("image/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), SELECT_FILE);
                    }
                    else if (args.Which == 2)
                    {
                        var intent = new Intent(this, typeof(GiphySearch));
                        this.StartActivityForResult(intent, GIPHY_SEARCH);
                    }
                });

                dialogBuilder.Show();
            }
        }
        public string BitmapToString(Bitmap bitmap)
        {
            byte[] bitmapData;
            int width = bitmap.Width > 800 ? 800 : bitmap.Width;
            int h = bitmap.Height * width / bitmap.Width;
            bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(bitmap, width, h, false);
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 0, stream);
                bitmapData = stream.ToArray();
            }
            return Convert.ToBase64String(bitmapData);
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                if (requestCode == REQUEST_CAMERA)
                {
                    // Handle the newly captured image
                    Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                    Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
                    mediaScanIntent.SetData(contentUri);
                    SendBroadcast(mediaScanIntent);

                    // Display in ImageView. We will resize the bitmap to fit the display.
                    // Loading the full sized image will consume to much memory
                    // and cause the application to crash.

                    //int height = Resources.DisplayMetrics.HeightPixels;
                    ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_respond_image_preview);
                    RelativeLayout img_preview_layout = FindViewById<RelativeLayout>(Resource.Id.comment_respond_image_preview_layout);
                    int width = 720;
                    App.bitmap = App._file.Path.LoadAndResizeBitmap(width);
                    if (App.bitmap != null)
                    {
                        img_preview_layout.Visibility = ViewStates.Visible;
                        int h = (int)App.bitmap.Height * 80 / App.bitmap.Width;
                        img_preview.SetImageBitmap(Bitmap.CreateScaledBitmap(App.bitmap, 80, h, false));
                        post_parts.Add(new PostPart2("image", BitmapToString(App.bitmap)));
                        App.bitmap = null;
                    }

                    // Dispose of the Java side bitmap.
                    GC.Collect();
                }
                else if (requestCode == SELECT_FILE)
                {
                    // Handle the image chosen from gallery
                    Android.Net.Uri uri = data.Data;

                    list_layout.AddView(GetLayout(this, "image", "", "", uri.ToString(), ""));
                    RelativeLayout rl = new RelativeLayout(this);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                    rl.LayoutParameters = p;
                    list_layout.AddView(rl);

                    App.bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, uri);
                    post_parts.Add(new PostPart2("image", BitmapToString(App.bitmap)));
                }
                else if (requestCode == GIPHY_SEARCH)
                {
                    if (data.HasExtra("image"))
                    {
                        App.bitmap = null;
                        string giphy_video = data.GetStringExtra("video");
                        string giphy_image = data.GetStringExtra("image");
                        list_layout.AddView(GetLayout(this, "gif", "", "", giphy_image, giphy_video));
                        RelativeLayout rl = new RelativeLayout(this);
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                        rl.LayoutParameters = p;
                        list_layout.AddView(rl);
                        post_parts.Add(new PostPart2("giphy", giphy_image,giphy_video));
                    }
                }
                else if(requestCode == AddTextCode)
                {
                    list_layout.AddView(GetLayout(this, "text", "", data.GetStringExtra("text"), "", ""));
                    RelativeLayout rl = new RelativeLayout(this);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                    rl.LayoutParameters = p;
                    list_layout.AddView(rl);
                    post_parts.Add(new PostPart2("text", data.GetStringExtra("text")));
                }
                else if(requestCode == AddVideoCode)
                {
                    string type = data.GetStringExtra("type");
                    if (type == "youtube" || type == "coub")
                    {
                        string id = data.GetStringExtra("video");
                        string image = "http://img.youtube.com/vi/"+id+ "/maxresdefault.jpg";
                        if (type == "coub")
                        {
                            string url = "http://www.exifeed.com/ajax_submit.php?get_coub_info=1&coub_id=" + id;
                            WebClient webclient = new WebClient();
                            image = Encoding.UTF8.GetString(webclient.DownloadData(url));
                        }
                        list_layout.AddView(GetLayout(this, type, id, "", image, ""));
                        RelativeLayout rl = new RelativeLayout(this);
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                        rl.LayoutParameters = p;
                        list_layout.AddView(rl);
                        post_parts.Add(new PostPart2(type, id));
                    }
                }
                else if (requestCode == SELECT_VIDEO)
                {
                    Android.Net.Uri uri = data.Data;
                    list_layout.AddView(GetLayout(this, "video", "", "", "", "",uri));
                    RelativeLayout rl = new RelativeLayout(this);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                    rl.LayoutParameters = p;
                    list_layout.AddView(rl);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    FileInputStream fis;
                    try
                    {

                        Java.IO.File file = new Java.IO.File(PostView.GetRealPathFromURI(this, uri));
                        System.Console.WriteLine(file.Length().ToString());
                        fis = new FileInputStream(file);

                        byte[] buf = new byte[1024];
                        int n;
                        while (-1 != (n = fis.Read(buf)))
                            baos.Write(buf, 0, n);
                    }
                    catch (Exception e)
                    {

                    }
                    byte[] bbytes = baos.ToByteArray();
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    post_parts.Add(new PostPart2("video", Convert.ToBase64String(bbytes)));                    
                }else if (requestCode == ADD_LINK)
                {
                    string html = "";
                    if (data.HasExtra("title"))
                    {
                        html = "<a href = '" + data.GetStringExtra("url") + "'>" + data.GetStringExtra("title") + "</a>";
                    }else
                    {
                        html = "<a href = '" + data.GetStringExtra("url") + "'>" + data.GetStringExtra("url") + "</a>";
                    }
                    list_layout.AddView(GetLayout(this, "text", "", html, "", ""));
                    RelativeLayout rl = new RelativeLayout(this);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                    rl.LayoutParameters = p;
                    list_layout.AddView(rl);
                    post_parts.Add(new PostPart2("text", html));
                }
            }
        }

        public RelativeLayout GetLayout(Context context,string type,string id,string title,string image,string video,Android.Net.Uri video_uri=null)
        {
            RelativeLayout rl = new RelativeLayout(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            rl.LayoutParameters = p;
            ImageView delete_button = new ImageView(context);
            p = new RelativeLayout.LayoutParams(50,50);
            delete_button.LayoutParameters = p;
            p.AddRule(LayoutRules.AlignParentRight);
            delete_button.SetImageResource(Resource.Drawable.delete_icon);
            delete_button.Click += (o,e)=>
            {
                ViewGroup viewgroup = (ViewGroup)rl.Parent;
                int index = viewgroup.IndexOfChild(rl);
                viewgroup.RemoveViewAt(index);
                viewgroup.RemoveViewAt(index);
                post_parts.RemoveAt(index / 2);
            };
            switch (type)
            {
                case "text":
                    {
                        TextView tv = new TextView(context);
                        tv.TextFormatted = Html.FromHtml(title);
                        tv.SetTextColor(Color.Black);
                        rl.AddView(tv);
                    }
                    break;
                case "image":
                    {
                        ImageView im = CreateImageView(image, context, type, id, video);
                        if (im != null)
                        {

                            rl.AddView(im);
                        }
                    }
                    break;
                case "gif":
                    {
                        ImageView im = CreateImageView(image, context, type, id, video);
                        RelativeLayout rl1 = new RelativeLayout(context);
                        rl1.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                        if (im != null)
                        {
                            rl1.AddView(im);
                            rl.AddView(rl1);
                            rl.AddView(CreatePlayButton(context));
                        }
                    }
                    //rl.AddView(button);
                    break;
                case "coub":
                    {
                        ImageView im = CreateImageView(image, context, type, id, video);
                        RelativeLayout rl1 = new RelativeLayout(context);
                        rl1.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                        if (im != null)
                        {
                            rl1.AddView(im);
                            rl.AddView(rl1);
                            rl.AddView(CreatePlayButton(context));
                        }
                    }
                    // rl.AddView(button);
                    break;
                case "youtube":
                    {
                        ImageView im = CreateImageView(image, context,type,id,video);
                        RelativeLayout rl1 = new RelativeLayout(context);
                        rl1.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                        if (im != null)
                        {
                            rl1.AddView(im);
                            rl.AddView(rl1);
                            rl.AddView(CreatePlayButton(context));
                        }
                    }
                    break;
                case "video":
                    VideoView v = new VideoView(this);
                    v.LayoutParameters = new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
                    v.SetVideoURI(video_uri);
                    v.SeekTo(100);
                    rl.AddView(v);
                    break;
            }
            rl.AddView(delete_button);
            return rl;
        }



        private ImageView CreateImageView(string Url, Context context,string type,string id,string video)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);
            im.LayoutParameters = p;
            im.SetAdjustViewBounds(true);
            im.SetScaleType(ImageView.ScaleType.CenterCrop);
            //Bitmap imageBitmap = GetImageBitmapFromUrl(Url);
            Koush.UrlImageViewHelper.SetUrlDrawable(im, Url);
            //im.SetImageBitmap(imageBitmap);
            im.Click += delegate
            {
                ImageClick(context, type, id, video);
            };
            return im;
        }
        public ImageView CreatePlayButton(Context context)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            im.LayoutParameters = p;
            p.AddRule(LayoutRules.CenterInParent);
            im.SetAdjustViewBounds(true);
            im.SetImageResource(Resource.Drawable.play_button);
            return im;
        }
        private void ImageClick(Context context, string type, string id, string video)
        {
            if (type == "gif")
            {
                //Toast.MakeText(context, video, ToastLength.Short).Show();
                Intent intent = new Intent(context, typeof(Video));
                intent.PutExtra("video", video);
                context.StartActivity(intent);

            }
            if (type == "youtube")
            {
                string url = "https://youtube.com/watch?v=" + id;
                context.StartActivity(new Intent(Intent.ActionView, Android.Net.Uri.Parse(url)));
            }
            if (type == "coub")
            {
                Intent intent = new Intent(context, typeof(Video));
                intent.PutExtra("video", video);
                // Toast.MakeText(context, audio, ToastLength.Short).Show();
                context.StartActivity(intent);
            }
        }

        public void ShowMessage(string message) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(message);
            alert.SetPositiveButton("Ok", (senderAlert, args) => {
            });
            RunOnUiThread(() => {
                alert.Show();
            });
        }
    }
}
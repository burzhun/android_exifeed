using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using Android.Util;
using Android.Graphics;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using Java.Lang;
using Xamarin.Facebook.Login;
using Org.Json;
using System.Collections.Specialized;

namespace AndroidGesture
{
    [Activity(Label = "Login", WindowSoftInputMode = SoftInput.AdjustPan)]
    public class Login : Activity,IFacebookCallback,GraphRequest.IGraphJSONObjectCallback
    {
        private bool captcha;
        TextView wrongName;
        ImageView captchaImage;
        EditText captchaText;
        private string url_user;
        private WebClient webClient;
        Button button;
        string hash;
        private ICallbackManager mCallBackManager;
        string facebook_user_id;
        EditText login_field;
        EditText password_field;
        bool login_pressed = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            FacebookSdk.SdkInitialize(this.ApplicationContext);

            SetContentView(Resource.Layout.login);
            mCallBackManager = CallbackManagerFactory.Create();
            LoginManager.Instance.RegisterCallback(mCallBackManager, this);
            RelativeLayout f_button = FindViewById<RelativeLayout>(Resource.Id.facebook_button);
            f_button.Click += (o, e) => {
                LoginManager.Instance.LogInWithReadPermissions(this, new List<string> { "public_profile" });
            };

            button = FindViewById<Button>(Resource.Id.loginButton);
            button.Click += button_click;
            
            wrongName = FindViewById<TextView>(Resource.Id.wrongName);
            captchaImage = FindViewById<ImageView>(Resource.Id.captchaImage);
            captchaText = FindViewById<EditText>(Resource.Id.captchaLogin);
            FindViewById<TextView>(Resource.Id.sign_up_link).Click += delegate{
                StartActivity(new Intent(this,typeof(SignUp)));
            };
            login_field = FindViewById<EditText>(Resource.Id.usernameLogin);
            password_field = FindViewById<EditText>(Resource.Id.passwordLogin);
            login_field.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    login_field.ClearFocus();
                    password_field.RequestFocus();
                }
                else
                {
                    e.Handled = false;
                }
            };
            password_field.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    if (password_field.Text != "")
                    {
                        password_field.ClearFocus();
                        button_click(null, null);
                    }                    
                }
                else
                {
                    e.Handled = false;
                }
            };
            captchaText.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    button_click(null, null);
                }
                else
                {
                    e.Handled = false;
                }
            };
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (UserParameters.retrieveset("token") != null)
            {
                Finish();
            }
        }

        private void button_click(object sender, EventArgs e)
        {
            Console.WriteLine("click");
            wrongName.Visibility = ViewStates.Gone;
            captchaText.Visibility = ViewStates.Gone;
            captchaImage.Visibility = ViewStates.Gone;
            string username = login_field.Text;
            string password = password_field.Text;           
            url_user = UrlManager.domain + "login?api_key=sdsgre345345hkb5kh345kj345";
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection["email"] = username;
            nameValueCollection["password"] = password;
            string captcha_string;
            if (captcha)
            {
                captcha_string = FindViewById<EditText>(Resource.Id.captchaLogin).Text;
                nameValueCollection["captcha"] = captcha_string;
                nameValueCollection["hash"] = hash;
            }
            captcha = false;
            Uri userToken = new Uri(url_user);
            webClient = new WebClient();
            webClient.UploadValuesAsync(userToken,"POST",nameValueCollection);
            webClient.UploadValuesCompleted += user_login;
        }
        

        private void user_login(object sender, UploadValuesCompletedEventArgs e)
        {
            RunOnUiThread(() =>
            {
                string json;
                try
                {
                   json  = Encoding.UTF8.GetString(e.Result);
                }catch(Java.Lang.Exception e1)
                {
                    Console.WriteLine(e1.ToString());
                    return;
                }                
                var jobject = Newtonsoft.Json.Linq.JObject.Parse(json);
                string text = jobject["text"].ToString();
                switch (text)
                {
                    case "wrong":
                        wrongName.Text = "Wrong login or password";
                        wrongName.Visibility = ViewStates.Visible;
                        break;
                    case "wrong_captcha":
                        wrongName.Text = "Enter correct code from image";
                        wrongName.Visibility = ViewStates.Visible;
                        captchaText.Visibility = captchaImage.Visibility = ViewStates.Visible;
                        break;
                    case "captcha":
                        string bytes = jobject["value"].ToString();
                        if(bytes!= "")
                        {
                            byte[] decodedString = Base64.Decode(bytes, Base64Flags.Default);
                            Bitmap decodedByte = BitmapFactory.DecodeByteArray(decodedString, 0, decodedString.Length);
                            captchaImage.SetImageBitmap(Bitmap.CreateScaledBitmap(decodedByte, 400, 200, false));
                        }
                        captchaText.Visibility = captchaImage.Visibility = ViewStates.Visible;
                        captchaText.Text = "";
                        captcha = true;
                        hash = jobject["hash"].ToString();
                        break;
                    case "token":
                        string token = jobject["value"].ToString();
                        UserParameters.saveset("token", token);
                        Finish();
                        break;

                }
            });
                
        }

        public void OnCancel()
        {
            Console.WriteLine("cancel");
            //throw new NotImplementedException();
        }

        public void OnError(FacebookException p0)
        {
            Console.WriteLine(p0.Message);
            if (p0 == null) return;
            Console.WriteLine("error");
            button.Text = "error";
           // Console.WriteLine(p0.Cause.ToString());
            //
            //throw new NotImplementedException();
        }

        public void OnSuccess(Java.Lang.Object p0)
        {
            LoginResult loginResult = p0 as LoginResult;
            facebook_user_id = loginResult.AccessToken.UserId;
            GraphRequest request = GraphRequest.NewMeRequest(AccessToken.CurrentAccessToken, this);
            Bundle parameters = new Bundle();
            parameters.PutString("fields", "id,name,email");
            request.Parameters = parameters;
            request.ExecuteAsync();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            mCallBackManager.OnActivityResult(requestCode, (int)resultCode, data);
        }

        public void OnCompleted(JSONObject p0, GraphResponse p1)
        {
            string picture_url = "https://graph.facebook.com/"+facebook_user_id+"/picture?width=150&height=150";
            var jobject = Newtonsoft.Json.Linq.JObject.Parse(p0.ToString());
            string url = UrlManager.domain + "add_facebook_user?api_key=sdsgre345345hkb5kh345kj345";
            Uri userToken = new Uri(url);
            webClient = new WebClient();
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection["username"] = jobject["name"].ToString();
            nameValueCollection["email"] = jobject["email"].ToString();
            nameValueCollection["facebook_id"] = jobject["id"].ToString();
            nameValueCollection["image"] = picture_url;
            byte[] result = webClient.UploadValues(url, "POST", nameValueCollection);
            Console.WriteLine(Encoding.UTF8.GetString(result));
            jobject = Newtonsoft.Json.Linq.JObject.Parse(Encoding.UTF8.GetString(result));
            string token = jobject["value"].ToString();
            Console.WriteLine(token);
            UserParameters.saveset("token", token);
            Finish();
        }
        
    }

   
}
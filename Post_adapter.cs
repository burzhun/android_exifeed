using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Exifeed;
using Android.Graphics;
using System.Net;
using Newtonsoft.Json;
using Plugin.Share;
using Android.Text;
using Android.Text.Style;

namespace AndroidGesture
{
    class Post_adapter : BaseAdapter<Post>
    {
        Context context;
        public List<Post> list;
        int ScreenWidth;
        bool login = false;
        public Post_adapter(List<Post> list1, Context context1)
        {
            list = list1;
            context = context1;
            var metrics = context.Resources.DisplayMetrics;
            ScreenWidth = metrics.WidthPixels;
            if (UserParameters.retrieveset("token")!=null)
            {
                login = true;
            }
            PostPart.last_played_index = 0;
        }
        public override Post this[int position]
        {
            get
            {
                return list[position];
            }
        }

        public override int Count
        {
            get
            {
                return list.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            Post post = list[position];
            PostViewHolder holder;

            if (convertView == null)
            {
                row = LayoutInflater.From(context).Inflate(Resource.Layout.post_layout, null, false);
                holder = new PostViewHolder();
                holder.post_title = row.FindViewById<TextView>(Resource.Id.postTitle);
                holder.description = row.FindViewById<TextView>(Resource.Id.description);
                holder.post_parts_layout = row.FindViewById<LinearLayout>(Resource.Id.postContainer);
                holder.save_post = row.FindViewById<ImageView>(Resource.Id.save_post);
                holder.share_layout = row.FindViewById<RelativeLayout>(Resource.Id.post_share_layout);
                holder.show_text = row.FindViewById<TextView>(Resource.Id.show_entire_post);
                holder.tags_layout = row.FindViewById<LinearLayout>(Resource.Id.tags_layout);
                holder.upvote = row.FindViewById<TextView>(Resource.Id.post_upvote_button);
                holder.userImage = row.FindViewById<ImageView>(Resource.Id.PostUserImage);
                holder.username = row.FindViewById<TextView>(Resource.Id.PostUsername);
                holder.addDate = row.FindViewById<TextView>(Resource.Id.PostDate);
                holder.comments_count = row.FindViewById<TextView>(Resource.Id.post_comments_count);
                holder.comments_layout = row.FindViewById<RelativeLayout>(Resource.Id.post_comments_button_layout);                
                row.Tag = holder;
            }
            else
            {
                holder = row.Tag as PostViewHolder;
            }
            LinearLayout l = new LinearLayout(context);
            l.Orientation = Orientation.Vertical;
            l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            holder.post_parts_layout.RemoveAllViews();
            List<PostPart> list2 = null;
            try
            {
                list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PostPart>>(post.text);
            } catch (Exception e1)
            {
                return row;
            }
            holder.tags_layout.RemoveAllViews();
            string[] tags = post.tags.Split(',');
            Color tags_color = new Color(Android.Support.V4.Content.ContextCompat.GetColor(context, Resource.Color.top_background));
            SpannableStringBuilder builder = new SpannableStringBuilder();
            int tag_start = 0;
            holder.tags_layout.Visibility = ViewStates.Gone;
            foreach (string tag in tags)
            {
                if (tag != "")
                {
                    var clickableSpan = new MyClickableSpan(tag,context);
                    builder.Append("#" + tag + "  ");
                    builder.SetSpan(clickableSpan, tag_start,tag_start+tag.Length+1,SpanTypes.ExclusiveExclusive);
                    tag_start += tag.Length + 3;
                }                
            }                    
            if(tag_start>0) holder.tags_layout.Visibility = ViewStates.Visible;
            TextView tag_view = new TextView(context);
            tag_view.MovementMethod = new Android.Text.Method.LinkMovementMethod();
            tag_view.Clickable = false;
            tag_view.Append(builder);
            tag_view.SetTextColor(tags_color);
            tag_view.SetTextSize(Android.Util.ComplexUnitType.Dip, 16);
            holder.tags_layout.AddView(tag_view);
            string userimage = post.userimage.Contains("http") ? post.userimage : "http://exifeed.com" + post.userimage;
            Koush.UrlImageViewHelper.SetUrlDrawable(holder.userImage, userimage);
            holder.username.Text = post.username;
            holder.addDate.Text = post.elapsed_time;
            if (post.show_full == 0) l.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            holder.show_text.Visibility = ViewStates.Gone;
            holder.show_text.Visibility = ViewStates.Gone;
            foreach (PostPart part in list2)
            {
                l.AddView(part.GetLayout(context,position));
                RelativeLayout rl = new RelativeLayout(context);
                RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 15);
                rl.LayoutParameters = p;
                l.AddView(rl);
            }
            holder.post_parts_layout.AddView(l);
            holder.post_title.Text =Html.FromHtml(list[position].title).ToString();         
            if (post.show_full == 1)
            {
                l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 1000);
                holder.show_text.Visibility = ViewStates.Visible;
                holder.show_text.Click += delegate {
                    post.show_full = 2;
                    holder.show_text.Visibility = ViewStates.Gone;
                    l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                };
            }
            else if(list2.Count<2){
                post.show_full = 2;
                holder.show_text.Visibility = ViewStates.Gone;
                l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            }else if(list2.Count>4)
            {
                post.show_full = 1;
                l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 1000);
                holder.show_text.Visibility = ViewStates.Visible;
                holder.show_text.Click += delegate {
                    post.show_full = 2;
                    holder.show_text.Visibility = ViewStates.Gone;
                    l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                };
            }
            else
            {
                l.ViewTreeObserver.AddOnGlobalLayoutListener(new GlobalLayoutListener(position,ref l, ref post, ref holder.show_text));            
            }
            holder.description.Visibility = ViewStates.Gone;
            if (post.description != "")
            {
                holder.description.Visibility = ViewStates.Visible;
                holder.description.Text = post.description;
            }
            holder.post_title.SetOnClickListener(new TitleClickListener(context,post.id.ToString()));
            holder.username.Click += delegate
            {
                Intent intent = new Intent(context, typeof(UserActivity)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("user_id", post.user_id.ToString());
                context.StartActivity(intent);
            };
            holder.userImage.Click += delegate
            {
                Intent intent = new Intent(context, typeof(UserActivity)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("user_id", post.user_id.ToString());
                context.StartActivity(intent);
            };
            holder.share_layout.SetOnClickListener(new ShareButtonClickListener(post.GetUrl(), post.title, context));
            holder.comments_count.Text = post.comments_count.ToString();
            holder.comments_layout.Click += delegate
            {
                Intent intent = new Intent(context, typeof(PostView)).SetFlags(ActivityFlags.ClearTop);
                intent.PutExtra("post_id", post.id.ToString());
                intent.PutExtra("to_comments", true);
                context.StartActivity(intent);
            };
            if (login)
            {
                holder.upvote.Visibility = ViewStates.Visible;
                if(post.user_rating == 1)
                {
                    String htmlString = "<u>" + post.rating + " Upvoted</u>";
                    holder.upvote.SetTextColor(Color.Rgb(107, 138, 80));
                    holder.upvote.TextFormatted = Html.FromHtml(htmlString);
                    holder.upvote.SetOnClickListener(new UpvoteButtonClickListener(post.id.ToString(), context,  ref holder.upvote, post.rating,true));
                }
                else
                {
                    String htmlString = "<u>" + post.rating + " Upvote</u>";
                    holder.upvote.SetTextColor(Color.Rgb(10,10,10));
                    holder.upvote.TextFormatted = Html.FromHtml(htmlString);
                    holder.upvote.SetOnClickListener(new UpvoteButtonClickListener(post.id.ToString(), context,  ref holder.upvote, post.rating));
                }
                holder.save_post.Visibility = ViewStates.Visible;
                if(post.saved == 1)
                {
                    holder.save_post.Alpha = 1;
                    holder.save_post.SetOnClickListener(new SaveButtonClickListener(post.id.ToString(), context,  ref holder.save_post ));
                }else
                {
                    holder.save_post.SetOnClickListener(new SaveButtonClickListener(post.id.ToString(), context, ref holder.save_post,true));
                    holder.save_post.Alpha = 0.4f;
                }
            }
            else
            {
                holder.save_post.Visibility = ViewStates.Gone;
                holder.upvote.Visibility = ViewStates.Gone;
            }

            // setWebView(ref w,list[position].text);
            //CrossShare.Current.Share()
            return row;
        }

        public class GlobalLayoutListener : Java.Lang.Object, ViewTreeObserver.IOnGlobalLayoutListener
        {
            Post post;
            LinearLayout l;
            TextView show_text;
            int p;
            public GlobalLayoutListener(int p1,ref LinearLayout l1,ref Exifeed.Post post1,ref TextView show_full1)
            {
                post = post1;
                l = l1;
                show_text = show_full1;
                p = p1;
            }
            public void OnGlobalLayout()
            {
                if (post.show_full == 0)
                {
                    if (l.Height > 1000)
                    {
                        l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 1000);
                        show_text.Visibility = ViewStates.Visible;
                        post.show_full = 1;
                        show_text.Click += delegate
                        {
                            post.show_full = 2;
                            show_text.Visibility = ViewStates.Gone;
                            l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                        };
                    }
                    else
                    {
                        l.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                        post.show_full = 2;
                    }
                }
            }
        }
    }

    public class PostViewHolder : Java.Lang.Object
    {
        public TextView post_title;
        public TextView description;
        public LinearLayout post_parts_layout;
        public LinearLayout tags_layout;
        public ImageView userImage;
        public TextView username;
        public TextView addDate;
        public ImageView save_post;
        public TextView show_text;
        public RelativeLayout share_layout;
        public TextView comments_count;
        public RelativeLayout comments_layout;
        public TextView upvote;
    }

    public class TitleClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string post_id;
        Context c;
        public TitleClickListener(Context c1,string id)
        {
            c = c1;
            post_id = id;
        }
        public void OnClick(View v)
        {
            //throw new NotImplementedException();
            Intent intent = new Intent(c, typeof(PostView));
            intent.PutExtra("post_id", post_id);
            c.StartActivity(intent);
        }
    }

    public class ShareButtonClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string url;
        string title;
        Context c;
        public ShareButtonClickListener(string Url,string Title, Context context)
        {
            url = Url;
            title = Title;
            c = context;
        }
        public void OnClick(View v)
        {
            CrossShare.Current.ShareLink(url, "", title);
        }
    }
    
    public class UpvoteButtonClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string post_id;
        Context c;
        TextView t;
        int r;
        bool down = false;
        public UpvoteButtonClickListener(string Post_id, Context context, ref TextView t1, int rating,bool d=false)
        {
            post_id = Post_id;
            c = context;
            t = t1;
            r = rating;
            down = d;
        }
        public void OnClick(View v)
        {
            if (down)
            {
                r--;
                String htmlString = "<u>" + r + " Upvote</u>";
                t.SetTextColor(Color.Rgb(10, 10, 10));
                t.TextFormatted = Html.FromHtml(htmlString);
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("post_id", post_id);
                dict.Add("rating", "0");
                UrlManager.PostRequest("post_rating", dict);
            }
            else
            {
                r++;
                String htmlString = "<u>" + r + " Upvoted</u>";
                t.SetTextColor(Color.Rgb(107, 138, 80));
                t.TextFormatted = Html.FromHtml(htmlString);
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("post_id", post_id);
                dict.Add("rating", "1");
                UrlManager.PostRequest("post_rating", dict);
            }
            down = !down;

        }
    }

    public class SaveButtonClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string post_id;
        Context c;
        ImageView t;
        bool save = false;
        public SaveButtonClickListener(string Post_id, Context context,  ref ImageView t1, bool d = false)
        {
            post_id = Post_id;
            c = context;
            t = t1;
            save = d;
        }
        public void OnClick(View v)
        {
            if (save)
            {
                t.Alpha = 1;
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("post_id", post_id);
                UrlManager.PostRequest("save_post", dict);
            }
            else
            {
                t.Alpha = 0.4f;
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("post_id", post_id);
                dict.Add("unsave", "1");
                UrlManager.PostRequest("save_post", dict);
            }
            save = !save;

        }
    }

    public class MyClickableSpan : ClickableSpan
    {
        public string tag;
        Context context;
        public MyClickableSpan(string t,Context c)
        {
            tag = t;
            context = c;
        }
        
        public override void OnClick(View widget)
        {
            var intent = new Intent(context, typeof(TagActivity));
            intent.PutExtra("tag", tag);
            context.StartActivity(intent);

        }
    }
}
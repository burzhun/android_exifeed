using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    [Activity(Label = "AddText")]
    public class AddText : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_text);
            // Create your application here
            EditText textview = FindViewById<EditText>(Resource.Id.add_post_text_view);
            TextView error = FindViewById<TextView>(Resource.Id.add_text_empty);
            Button add = FindViewById<Button>(Resource.Id.add_text_to_post_button);
            add.Click += delegate {
                error.Visibility = ViewStates.Gone;
                string text = textview.Text;
                if (text != "")
                {
                    Intent intent = new Intent();
                    intent.PutExtra("text", text);
                    ((Activity)this).SetResult(Result.Ok, intent);
                    ((Activity)this).Finish();
                }else
                {
                    error.Visibility = ViewStates.Visible;
                }
            };
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exifeed;
using Android.Graphics;

namespace AndroidGesture
{
    [Activity(Label = "TagActivity")]
    public class TagActivity : Activity
    {
        ListView listview;
        List<Post> list=null;
        Post_adapter post_adapter;
        string tag;
        int offset=0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.user);
            // Create your application here
            listview = FindViewById<ListView>(Resource.Id.user_postList);
            listview.Scroll += list_scrolled;
            tag = Intent.GetStringExtra("tag");
            if (tag == null || tag == "") Finish();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("tag", tag);
            dict.Add("type", "tag");
            var awaiter = UrlManager.PostRequestAsync("posts", dict).GetAwaiter();
            awaiter.OnCompleted(() => {
                string json = awaiter.GetResult();
                if(json!="error"&& json != "")
                {
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(awaiter.GetResult());
                    post_adapter = new Post_adapter(list, this);
                    listview.Adapter = post_adapter;
                    offset += 10;
                }                
            });
            View row = LayoutInflater.From(this).Inflate(Resource.Layout.tag_layout, null, false);
            Android.Text.SpannableStringBuilder builder = new Android.Text.SpannableStringBuilder();
            string s = "Posts with tag ";
            builder.Append(s);
            builder.Append("#" + tag);
            var for_color = new Android.Text.Style.ForegroundColorSpan(new Color(Android.Support.V4.Content.ContextCompat.GetColor(this, Resource.Color.top_background)));
            builder.SetSpan(for_color, s.Length, s.Length + tag.Length + 1, Android.Text.SpanTypes.ExclusiveExclusive);
            row.FindViewById<TextView>(Resource.Id.tag_label).Append(builder);
            listview.AddHeaderView(row);

        }

        private void list_scrolled(object sender, AbsListView.ScrollEventArgs e)
        {
            if (list == null) return;
            if(e.FirstVisibleItem + e.VisibleItemCount+1 >= offset)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("tag", tag);
                dict.Add("type", "tag");
                dict.Add("offset", offset.ToString());
                var awaiter = UrlManager.PostRequestAsync("posts", dict).GetAwaiter();
                awaiter.OnCompleted(() => {
                    List<Post> list1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(awaiter.GetResult());
                    list.AddRange(list1);
                    post_adapter.NotifyDataSetChanged();
                });
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    class UserParameters
    {
        public static Dictionary<string, string> video_sizes = new Dictionary<string, string>();
        public static void saveset(string key,string value)
        {

            //store
            var prefs = Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(key, value);
            prefEditor.Commit();

        }

        // Function called from OnCreate
        public static string retrieveset(string key)
        {
            //retreive 
            var prefs = Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private);
            var somePref = prefs.GetString(key, null);
            return somePref;
        }

       public static void AddVideoSize(string video_url,int width,int height)
        {

            if (!video_sizes.ContainsKey(video_url)&&height>0)
            {
                video_sizes.Add(video_url, width.ToString() + "|" + height.ToString());
            }
        } 

        public static int[] GetVideoSize(string video_url)
        {
            return video_sizes[video_url].Split('|').Select(item => Convert.ToInt32(item)).ToArray<int>();
        }
    }
}
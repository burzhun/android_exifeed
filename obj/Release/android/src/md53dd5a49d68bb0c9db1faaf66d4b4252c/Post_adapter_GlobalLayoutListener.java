package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class Post_adapter_GlobalLayoutListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.ViewTreeObserver.OnGlobalLayoutListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onGlobalLayout:()V:GetOnGlobalLayoutHandler:Android.Views.ViewTreeObserver/IOnGlobalLayoutListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.Post_adapter+GlobalLayoutListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", Post_adapter_GlobalLayoutListener.class, __md_methods);
	}


	public Post_adapter_GlobalLayoutListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == Post_adapter_GlobalLayoutListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.Post_adapter+GlobalLayoutListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onGlobalLayout ()
	{
		n_onGlobalLayout ();
	}

	private native void n_onGlobalLayout ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class UpvoteButtonClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.UpvoteButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", UpvoteButtonClickListener.class, __md_methods);
	}


	public UpvoteButtonClickListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == UpvoteButtonClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.UpvoteButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public UpvoteButtonClickListener (java.lang.String p0, android.content.Context p1, android.widget.TextView p2, int p3, boolean p4) throws java.lang.Throwable
	{
		super ();
		if (getClass () == UpvoteButtonClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.UpvoteButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Widget.TextView&, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.Boolean, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2, p3, p4 });
	}


	public void onClick (android.view.View p0)
	{
		n_onClick (p0);
	}

	private native void n_onClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

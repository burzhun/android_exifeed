package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class VideoLoop1
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.media.MediaPlayer.OnPreparedListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onPrepared:(Landroid/media/MediaPlayer;)V:GetOnPrepared_Landroid_media_MediaPlayer_Handler:Android.Media.MediaPlayer/IOnPreparedListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.VideoLoop1, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", VideoLoop1.class, __md_methods);
	}


	public VideoLoop1 () throws java.lang.Throwable
	{
		super ();
		if (getClass () == VideoLoop1.class)
			mono.android.TypeManager.Activate ("AndroidGesture.VideoLoop1, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public VideoLoop1 (android.widget.MediaController p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == VideoLoop1.class)
			mono.android.TypeManager.Activate ("AndroidGesture.VideoLoop1, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Widget.MediaController&, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public void onPrepared (android.media.MediaPlayer p0)
	{
		n_onPrepared (p0);
	}

	private native void n_onPrepared (android.media.MediaPlayer p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class TextViewClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.TextViewClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", TextViewClickListener.class, __md_methods);
	}


	public TextViewClickListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == TextViewClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.TextViewClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public TextViewClickListener (android.content.Context p0, java.lang.String p1) throws java.lang.Throwable
	{
		super ();
		if (getClass () == TextViewClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.TextViewClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1 });
	}


	public void onClick (android.view.View p0)
	{
		n_onClick (p0);
	}

	private native void n_onClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

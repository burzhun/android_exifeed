package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class ShareButtonClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.ShareButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ShareButtonClickListener.class, __md_methods);
	}


	public ShareButtonClickListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ShareButtonClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.ShareButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public ShareButtonClickListener (java.lang.String p0, java.lang.String p1, android.content.Context p2) throws java.lang.Throwable
	{
		super ();
		if (getClass () == ShareButtonClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.ShareButtonClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public void onClick (android.view.View p0)
	{
		n_onClick (p0);
	}

	private native void n_onClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

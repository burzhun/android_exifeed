package md53dd5a49d68bb0c9db1faaf66d4b4252c;


public class PlayButtonLongClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnLongClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onLongClick:(Landroid/view/View;)Z:GetOnLongClick_Landroid_view_View_Handler:Android.Views.View/IOnLongClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("AndroidGesture.PlayButtonLongClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", PlayButtonLongClickListener.class, __md_methods);
	}


	public PlayButtonLongClickListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == PlayButtonLongClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.PlayButtonLongClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public PlayButtonLongClickListener (java.lang.String p0, android.content.Context p1, boolean p2) throws java.lang.Throwable
	{
		super ();
		if (getClass () == PlayButtonLongClickListener.class)
			mono.android.TypeManager.Activate ("AndroidGesture.PlayButtonLongClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Boolean, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public boolean onLongClick (android.view.View p0)
	{
		return n_onLongClick (p0);
	}

	private native boolean n_onLongClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

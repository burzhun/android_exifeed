package mono.com.google.android.exoplayer2.audio;


public class AudioTrack_ListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.google.android.exoplayer2.audio.AudioTrack.Listener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onUnderrun:(IJJ)V:GetOnUnderrun_IJJHandler:Com.Google.Android.Exoplayer2.Audio.AudioTrack/IListenerInvoker, ExoPlayer\n" +
			"";
		mono.android.Runtime.register ("Com.Google.Android.Exoplayer2.Audio.AudioTrack+IListenerImplementor, ExoPlayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", AudioTrack_ListenerImplementor.class, __md_methods);
	}


	public AudioTrack_ListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == AudioTrack_ListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Google.Android.Exoplayer2.Audio.AudioTrack+IListenerImplementor, ExoPlayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onUnderrun (int p0, long p1, long p2)
	{
		n_onUnderrun (p0, p1, p2);
	}

	private native void n_onUnderrun (int p0, long p1, long p2);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

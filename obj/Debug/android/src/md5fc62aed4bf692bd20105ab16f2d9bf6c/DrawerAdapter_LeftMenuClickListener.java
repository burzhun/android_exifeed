package md5fc62aed4bf692bd20105ab16f2d9bf6c;


public class DrawerAdapter_LeftMenuClickListener
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.View.OnClickListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("Exifeed.DrawerAdapter+LeftMenuClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", DrawerAdapter_LeftMenuClickListener.class, __md_methods);
	}


	public DrawerAdapter_LeftMenuClickListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == DrawerAdapter_LeftMenuClickListener.class)
			mono.android.TypeManager.Activate ("Exifeed.DrawerAdapter+LeftMenuClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public DrawerAdapter_LeftMenuClickListener (java.lang.String p0, android.content.Context p1) throws java.lang.Throwable
	{
		super ();
		if (getClass () == DrawerAdapter_LeftMenuClickListener.class)
			mono.android.TypeManager.Activate ("Exifeed.DrawerAdapter+LeftMenuClickListener, AndroidGesture, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public void onClick (android.view.View p0)
	{
		n_onClick (p0);
	}

	private native void n_onClick (android.view.View p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Interop;
using Android.Webkit;

namespace AndroidGesture
{
    
    
    class MyJSInterface : Java.Lang.Object
    {
        Context context;

        public MyJSInterface(Context context)
        {
            this.context = context;
        }
        [Export]
        [JavascriptInterface]
        public void ShowToast()
        {
            Toast.MakeText(context, "Hello from C#", ToastLength.Short).Show();
        }
        [Export]
        [JavascriptInterface]
        public void ShowPost(Java.Lang.String number)
        {
            var intent = new Intent(this.context, typeof(PostView));
            intent.PutExtra("Post_id", number.ToString());
            this.context.StartActivity(intent);
            //Toast.MakeText(context, "Hello from C#"+number.ToString(), ToastLength.Short).Show();
        }
        [Export]
        [JavascriptInterface]
        public void ShowVideo(Java.Lang.String url)
        {
            var intent = new Intent(this.context, typeof(Video));
            intent.PutExtra("url", url.ToString());
            this.context.StartActivity(intent);
        }
    }


}
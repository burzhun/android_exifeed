using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Exifeed
{
    class Post
    {
        public int id;
        public string title;
        public string description;
        public string text;
        public int user_id;
        public string username;
        public string userimage;
        public string elapsed_time;
        public int comments_count;
        public int rating;
        public int user_rating;
        public int saved;
        public int show_full;
        public string tags;
        public Post(int Id,string Title,string Description,string Text,int User_id,string Username,string Userimage,
            string Elapsed_time, int Com_count,int Rating,int User_rating,int Saved,string Tags)
        {
            id = Id;
            title = Title;
            description = Description;
            text = Text;
            user_id = User_id;
            username = Username;
            userimage = Userimage;
            elapsed_time = Elapsed_time;
            show_full = 0;//0-unknown,1-not full,2-full
            comments_count = Com_count;
            rating = Rating;
            user_rating = User_rating;
            saved = Saved;
            tags = Tags;
        }

        public string GetUrl()
        {
            return "http://www.exifeed.com/post/id" + this.id;
        }
    }
}
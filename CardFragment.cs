using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using Exifeed;
using Android.Support.V4.Widget;

namespace AndroidGesture
{
    [Activity(Label = "CardFragment")]
    public class CardFragment : Android.Support.V4.App.Fragment
    {

        private int position;
        private string[] Titles = { "Top", "New", "Best","Feed" };
        private List<Post>[] list = new List<Post>[4];
        private SwipeRefreshLayout refreshLayout;
        Post_adapter[] adapter = new Post_adapter[4];
        ListView listView;
        public Context context;
        string token;
        List<string> first_load;
        public CardFragment(Context c,string s)
        {
            context = c;
            first_load = Newtonsoft.Json.Linq.JArray.Parse(s).Select(item => (string)item).ToList<string>();
        }
        public static CardFragment NewInstance(int position,Context c,string s)
        {
            var f = new CardFragment(c,s);
            var b = new Bundle();
            b.PutInt("position", position);
            f.Arguments = b;
            return f;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            token = UserParameters.retrieveset("token");
            position = Arguments.GetInt("position");
        }
        public override void OnResume()
        {
            base.OnResume();
            if (token!= UserParameters.retrieveset("token"))
            {
                MyScrollListener.DownloadPosts(ref listView, ref list, ref adapter,ref context, "Feed", 3, 0);
            }
        }
        public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Bundle savedInstanceState)
        {
            var root = inflater.Inflate(Resource.Layout.post_list_layout, container, false);
            listView = root.FindViewById<ListView>(Resource.Id.postListView);
            refreshLayout = root.FindViewById<SwipeRefreshLayout>(Resource.Id.refreshLayout);
            refreshLayout.Refresh +=(o,e)=> RefreshLayout_Refresh(position,refreshLayout,listView);
            string type = Titles[position];
            //MyScrollListener.DownloadPosts(ref listView, ref list, ref adapter,ref context, type, position, 0);
            var awaiter = Newtonsoft.Json.JsonConvert.DeserializeObjectAsync<List<Post>>(first_load[position]).GetAwaiter();
            awaiter.OnCompleted(()=> {                
                list[position] = awaiter.GetResult();
                adapter[position] = new Post_adapter(list[position], context);
                listView.Adapter = adapter[position];
                listView.SetOnScrollListener(new MyScrollListener(ref listView, context, position, ref list, ref adapter));
            });                       
            return root;
        }

        private void RefreshLayout_Refresh(int position,SwipeRefreshLayout refreshLaoyut1,ListView lv)
        {
            //Data Refresh Place  
            System.ComponentModel.BackgroundWorker work = new System.ComponentModel.BackgroundWorker();
            work.DoWork += (o,e)=> MyScrollListener.DownloadPosts(ref lv, ref list, ref adapter, ref context, Titles[position], position, 0);
            work.RunWorkerCompleted += (o, e) => refreshLaoyut1.Refreshing = false;
            work.RunWorkerAsync();
        }

    }

    class MyScrollListener : Java.Lang.Object, AbsListView.IOnScrollListener
    {
        ListView listView;
        Context c;
        int position;
        private string[] Titles = { "Top", "New", "Best","Feed" };
        private List<Post>[] list = new List<Post>[4];
        Post_adapter[] adapter = new Post_adapter[4];
        public MyScrollListener(ref ListView lview, Context context, int p, ref List<Post>[] list1, ref Post_adapter[] adapter1)
        {
            listView = lview;
            c = context;
            position = p;
            list = list1;
            adapter = adapter1;
        }
        public void OnScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
        {
            if (list[position]==null || list[position].Count == 0) return;
            if (Math.Abs(firstVisibleItem - PostPart.last_played_index) > 1)
            {
                if (PostPart.player != null)
                {
                    //PostPart.player.PlayWhenReady = false;
                      PostPart.player.Stop();
                    // PostPart.player.SeekTo(0);
                    PostPart.player = null;
                    PostPart.videoSource = null;
                }
            }
            if (firstVisibleItem + visibleItemCount + 1 >= list[position].Count)
            {
                DownloadPosts(ref listView, ref list, ref adapter, ref c,Titles[position], position, listView.Count);
            }
        }

        public void OnScrollStateChanged(AbsListView view, [GeneratedEnum] ScrollState scrollState)
        {
            //throw new NotImplementedException();
        }

        public static void DownloadPosts(ref ListView listView, ref List<Post>[] list, ref Post_adapter[] adapter,ref Context c, string type, int position, int offset1 = 0, int limit1 = 10)
        {
            WebClient webClient = new WebClient();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            if (type != "")
            {
                dict.Add("type", type);
            }
            if (offset1 != 0)
            {
                dict.Add("offset", offset1.ToString());
            }
            if (limit1 != 10)
            {
                dict.Add("limit", limit1.ToString());
            }
            string json = UrlManager.PostRequest("posts", dict);
            if (json != "error" && json != "")
            {
                listView.Visibility = ViewStates.Visible;
                if (offset1 == 0)
                {
                    // list[position] = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);  
                    if(adapter[position] == null)
                    {
                        adapter[position] = new Post_adapter(list[position], c);
                    }   
                    adapter[position].list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                }
                else
                {
                    List<Post> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                    list[position].AddRange(list2);
                }
                adapter[position].NotifyDataSetChanged();
            }
        }
        
    }
}
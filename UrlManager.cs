using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Threading.Tasks;

namespace AndroidGesture
{
    class UrlManager
    {
        public static string domain = "http://www.exifeed.com/api.php/";
        public static string api_key = "sdsgre345345hkb5kh345kj345";

        public static string PostRequest(string url,Dictionary<string, string> array)
        {
            WebClient webclient = new WebClient();
            NameValueCollection coll = new NameValueCollection();
            if(array != null)
            {
                foreach (KeyValuePair<string, string> key in array)
                {
                    coll[key.Key] = key.Value;
                }
            }            
            if (UserParameters.retrieveset("token") != null)
            {
                coll["token"] = UserParameters.retrieveset("token");
            }
            coll["app_token"] = UserParameters.retrieveset("app_token");
            url = domain + url + "?api_key=" + api_key;
            try
            {
                string json = Encoding.UTF8.GetString(webclient.UploadValues(url, "POST", coll));
                Console.WriteLine(json);
                return json;
            }catch(Exception e1)
            {
                return "";
            }


        }

        public static void Post()
        {
            string url = "http://www.exifeed.com/test.php ";
            var webClient = new WebClient();
            byte[] byteArray = webClient.DownloadData("http://s.storage.akamai.coub.com/get/b7/p/coub/simple/cw_image/74ad4c4e01c/2f1634721661b71e7261a/med_1480804074_00032.jpg");
            NameValueCollection coll = new NameValueCollection();
            // Write the data to the request stream.     
            int len = 1000;
            int num = byteArray.Length / len;
            for (int i = 0; i <= num; i++)
            {
                if (i < num)
                {
                    byte[] ar = new byte[len];
                    Array.Copy(byteArray, i * len, ar, 0, len);
                    coll[i.ToString()] = Convert.ToBase64String(ar);
                }
                else
                {
                    byte[] ar = new byte[byteArray.Length % len];
                    Array.Copy(byteArray, i * len, ar, 0,byteArray.Length % len);
                    coll[i.ToString()] = Convert.ToBase64String(ar);
                }

            }
                
        }

        public static Task<string> PostRequestAsync(string url, Dictionary<string, string> array)
        {
            WebClient webclient = new WebClient();
            NameValueCollection coll = new NameValueCollection();
            if (array != null)
            {
                foreach (KeyValuePair<string, string> key in array)
                {
                    coll[key.Key] = key.Value;
                }
            }
            if (UserParameters.retrieveset("token") != null)
            {
                coll["token"] = UserParameters.retrieveset("token");
            }
            coll["app_token"] = UserParameters.retrieveset("app_token");
            url = domain + url + "?api_key=" + api_key;
            var tcs = new TaskCompletionSource<string>();
            webclient.UploadValuesAsync(new Uri(url), "POST", coll);
            webclient.UploadValuesCompleted += (o, e) => { tcs.SetResult(Encoding.UTF8.GetString(e.Result)); };
            return tcs.Task;
        }
    }
}
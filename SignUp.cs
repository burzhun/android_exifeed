using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.ComponentModel.DataAnnotations;
using System.Collections.Specialized;
using System.Net;

namespace AndroidGesture
{
    [Activity(Label = "SignUp")]
    public class SignUp : Activity
    {
        TextView errorView;
        string url;
        TextView username;
        TextView email;
        TextView password;
        Button button;
        EmailAddressAttribute email_validator;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.sign_up);
            // Create your application here

            username = FindViewById<TextView>(Resource.Id.sign_up_name);
            email = FindViewById<TextView>(Resource.Id.sign_up_email);
            password = FindViewById<TextView>(Resource.Id.sign_up_password);
            errorView = FindViewById<TextView>(Resource.Id.sign_up_error);
            button = FindViewById<Button>(Resource.Id.sign_up_button);
            email_validator = new EmailAddressAttribute();
            url = "http://www.exifeed.com/api.php/registration?api_key=sdsgre345345hkb5kh345kj345";
            username.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    username.ClearFocus();
                    email.RequestFocus();
                } else  { e.Handled = false; }
            };
            email.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    if(email.Text != "")
                    {
                        email.ClearFocus();
                        password.RequestFocus();
                    }                    
                }
                else { e.Handled = false; }
            };
            password.KeyPress += (o, e) => {
                if (e.KeyCode == Keycode.Enter)
                {
                    if(password.Text != "")
                    {
                        button_click();
                    }
                }
                else { e.Handled = false; }
            };
            button.Click += delegate {
                button_click();
            };
        }
        private void button_click()
        {
            errorView.Visibility = ViewStates.Gone;
            if (username.Text == "") { ShowError("Username can't be empty"); return; }
            if (email.Text == "") { ShowError("Email can't be empty"); return; }
            if (password.Text == "") { ShowError("Password can't be empty"); return; }
            if (!email_validator.IsValid(email.Text)) { ShowError("Email is not valid"); return; }
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection["email"] = email.Text;
            nameValueCollection["username"] = username.Text;
            nameValueCollection["password"] = password.Text;
            WebClient webClient = new WebClient();
            string result = Encoding.UTF8.GetString(webClient.UploadValues(url, "POST", nameValueCollection));
            try
            {
                var json = Newtonsoft.Json.Linq.JObject.Parse(result);
                if (json["text"].ToString() != "token")
                {
                    ShowError(json["text"].ToString());
                }
                else
                {
                    UserParameters.saveset("token", json["value"].ToString());
                    Finish();
                }
            }
            catch (Exception e1)
            {

            }
        }
        public void ShowError(string error)
        {
            errorView.Visibility = ViewStates.Visible;
            errorView.Text = error;
        }
    }
}
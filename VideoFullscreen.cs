using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Threading;
using Android.Media;

namespace AndroidGesture
{
    [Activity(Label = "VideoFullscreen", Theme ="@android:style/Theme.NoTitleBar.Fullscreen")]
    public class VideoFullscreen : Activity
    {
        int seek;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.video_fullscreen);            
            String url = Intent.GetStringExtra("video");
            VideoView video1 = FindViewById<VideoView>(Resource.Id.videoView1);
            if (Intent.HasExtra("width"))
            {
                int width = Intent.GetIntExtra("width", 0);
                int height = Intent.GetIntExtra("height", 0);
                if (width * height != 0)
                {
                    height = height * (width / 400);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(400, height);
                    video1.LayoutParameters = p;
                    p.AddRule(LayoutRules.CenterInParent);
                }
            }
            MediaController mediaController = new MediaController(this);
            mediaController.SetAnchorView(video1);
            video1.SetMediaController(mediaController);
            video1.SetVideoURI(Android.Net.Uri.Parse(url));
            video1.Start();
            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.overlayLayout);
            bool playing = true;
            video1.SetOnPreparedListener(new VideoLoop1(ref mediaController));
            video1.Prepared += delegate
            {
                mediaController.Show(3000);
            };
           
            rl.Click += delegate
            {
                mediaController.Show(3000);
                // Finish();
            };
        }
    }

    public class VideoLoop1 : Java.Lang.Object, Android.Media.MediaPlayer.IOnPreparedListener
    {
        MediaController mc;
        public VideoLoop1(ref MediaController mc1)
        {
            mc = mc1;
        }
        
        public void OnPrepared(MediaPlayer mp)
        {
            mp.Looping = true;
            mc.Show(3000);
        }
    }
}
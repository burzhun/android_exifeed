using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Specialized;
using System.Net;
using System.IO;

namespace AndroidGesture
{
    class Comment
    {
        public string id;
        public string level;
        public string text;
        public string username;
        public string user_id;
        public string userimage;
        public string post_id;
        public string image_url;
        public string video_url;
        public string rating;
        public string post_title;
        public string parent_comment_username;
        public string parent_comment_userimage;
        public string parent_comment_text;
        public string parent_comment_image;
        public string parent_comment_video;
        public string parent_comment_user_id;
        public int seen;
        public Comment(string Id,string Level,string Text,string User_id,string Username,string Post_id,int Seen,
            string Image_url,string Video_url,string Rating,string Parent_comment_username,string Post_title,
            string Parent_comment_userimage, string Parent_comment_text, string Parent_comment_image, string Parent_comment_video,string Parent_comment_user_id)
        {
            id = Id;
            level = Level;
            text = Text;
            user_id = User_id;
            username = Username;
            image_url = Image_url;
            video_url = Video_url;
            post_id = Post_id;
            seen = Seen;
            rating = Rating;
            post_title = Post_title;
            parent_comment_userimage = Parent_comment_userimage;
            parent_comment_username = Parent_comment_username;
            parent_comment_text = Parent_comment_text;
            parent_comment_image = Parent_comment_image;
            parent_comment_video = Parent_comment_video;
            parent_comment_user_id = Parent_comment_user_id;
        }

        public static bool add_comment(string token,string post_id,string parent_comment_id,string text, Android.Graphics.Bitmap bitmap,string video,string giphy_image,string giphy_video)
        {
            Uri address = new Uri("http://www.exifeed.com/api.php/comment_respond?api_key=sdsgre345345hkb5kh345kj345");
            var webClient = new WebClient();
            NameValueCollection nameValueCollection = new NameValueCollection();
            nameValueCollection["Text"] = text;
            if (bitmap != null)
            {
                byte[] bitmapData;
                int width = bitmap.Width > 800 ? 800 : bitmap.Width;
                int h = bitmap.Height * width / bitmap.Width;
                bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(bitmap, width, h, false);
                using (var stream = new MemoryStream())
                {
                    bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 0, stream);
                    bitmapData = stream.ToArray();
                }
                nameValueCollection["Image"] = Convert.ToBase64String(bitmapData);
            }else if(video != null)
            {
                nameValueCollection["video"] = video;
            }
            else if (giphy_image != "")
            {
                nameValueCollection["giphy_image"] = giphy_image;
                nameValueCollection["giphy_video"] = giphy_video;
            }
            nameValueCollection["token"] = token;
            nameValueCollection["post_id"] = post_id;
            nameValueCollection["comment_id"] = parent_comment_id;
            byte[] result=webClient.UploadValues(address, "POST", nameValueCollection);
            try
            {
                string json = Encoding.UTF8.GetString(result);
                Console.WriteLine(json);
                if (json == "success")
                {
                    return true;
                }
            }
            catch(Exception e1)
            {
                return false;
            }            
            
            return false;
            //System.Console.WriteLine(UploadBitmapAsync(App.bitmap));
        }

    }
}
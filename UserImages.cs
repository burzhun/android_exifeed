using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Specialized;

namespace AndroidGesture
{
    [Activity(Label = "UserImages")]
    public class UserImages : Activity
    {
        ListView listview;
        List<GiphySearchResult> list;
        GiphyAdapter adapter;
        int lastposition = 0;
        int offset = 0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.user_images);
            // Create your application here
            listview = FindViewById<ListView>(Resource.Id.user_images_list);
            string json = UrlManager.PostRequest("userimages", null);
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GiphySearchResult>>(json);
            adapter = new GiphyAdapter(list, this);
            listview.Adapter = adapter;
            listview.Scroll += scroll;
            listview.ItemClick += click;
        }

        private void click(object sender, AdapterView.ItemClickEventArgs e)
        {
            string url = list[e.Position].video;
            if (url == "") return;
            Intent intent = new Intent(this, typeof(Video)).SetFlags(ActivityFlags.ClearTop);
            try
            {
                ImageView image = listview.GetChildAt(e.Position).FindViewById<ImageView>(Resource.Id.giphy_image);
                int height = image.Height;
                int width = image.MeasuredWidth;
                intent.PutExtra("width", width);
                intent.PutExtra("height", height);
            }
            catch (Exception e1)
            { }
            // TextView textview = listview.GetChildAt(e.Position).FindViewById<TextView>(Resource.Id.giphy_add_button);
            intent.PutExtra("video", url);
            this.StartActivity(intent);


        }

        private void scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (list == null) return;
            if (e.View.LastVisiblePosition >= list.Count - 1)
            {
                offset += list.Count;
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("offset", offset.ToString());
                try
                {
                    string json = UrlManager.PostRequest("userimages", dict);
                    List<GiphySearchResult> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GiphySearchResult>>(json);
                    list.AddRange(list2);
                    adapter.NotifyDataSetChanged();
                }
                catch (Exception e1)
                {
                    return;
                }
            }

        }
    }
}
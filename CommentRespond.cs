using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using System.Net.Http.Headers;
using System.Net.Http;
using Android.Provider;
using Java.IO;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace AndroidGesture
{
    [Activity(Label = "CommentRespond")]
    public class CommentRespond : Activity
    {
        string comment_id;
        string post_id;
        string giphy_video, giphy_image;
        LinearLayout img_preview_layout;
        ImageView img_preview;
        VideoView video_preview;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.comment_respond);
            // Create your application here
            View row = LayoutInflater.From(this).Inflate(Resource.Layout.comment, null, false);
            comment_id = Intent.GetStringExtra("comment_id");
            post_id = Intent.GetStringExtra("post_id");
            Comment comment = getComment(comment_id);
            RelativeLayout parentRl = row.FindViewById<RelativeLayout>(Resource.Id.parent_comment_layout);
            parentRl.Visibility = ViewStates.Gone;
            if (comment.level != "1")
            {
                parentRl.Visibility = ViewStates.Visible;
                TextView p_username = row.FindViewById<TextView>(Resource.Id.parent_comment_username);
                p_username.Text = comment.parent_comment_username;
                ImageView p_userimage = row.FindViewById<ImageView>(Resource.Id.parent_comment_userimage);
                Koush.UrlImageViewHelper.SetUrlDrawable(p_userimage, comment.userimage);
                RelativeLayout parentComRL = row.FindViewById<RelativeLayout>(Resource.Id.parent_comment_container);
                parentComRL.RemoveAllViews();
                if (comment.parent_comment_text != "")
                {
                    TextView tv = new TextView(this);
                    tv.Text = comment.parent_comment_text;
                    parentComRL.AddView(tv);
                }
                if (comment.parent_comment_image != "")
                {
                    ImageView im = new ImageView(this);
                    im.SetScaleType(ImageView.ScaleType.FitStart);
                    Koush.UrlImageViewHelper.SetUrlDrawable(im, comment.parent_comment_image);
                    parentComRL.AddView(im);
                }
            }
            TextView username = row.FindViewById<TextView>(Resource.Id.comment_username);
            username.Text = comment.username;
            ImageView userimage = row.FindViewById<ImageView>(Resource.Id.comment_image);
            Koush.UrlImageViewHelper.SetUrlDrawable(userimage, comment.userimage);
            LinearLayout commentRL = row.FindViewById<LinearLayout>(Resource.Id.comment_container);
            commentRL.RemoveAllViews();
            img_preview_layout = FindViewById<LinearLayout>(Resource.Id.comment_respond_image_preview_layout);
            if (comment.text != "")
            {
                TextView tv = new TextView(this);
                tv.SetTextColor(Android.Graphics.Color.Rgb(30, 30, 30));
                tv.Text = comment.text;
                tv.SetTextSize(Android.Util.ComplexUnitType.Dip, 18);
                commentRL.AddView(tv);
            }
            if (comment.image_url != "")
            {
                ImageView im = new ImageView(this);
                Koush.UrlImageViewHelper.SetUrlDrawable(im, comment.image_url);
                if (comment.video_url == "")
                {
                    commentRL.AddView(im);
                }
                else
                {
                    RelativeLayout im_layout = new RelativeLayout(this);
                    im_layout.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                    im_layout.AddView(im);
                    im_layout.AddView(CreatePlayButton(this, comment.video_url));
                    commentRL.AddView(im_layout);
                }

            }
            video_preview = FindViewById<VideoView>(Resource.Id.comment_respond_video_preview);
            img_preview = FindViewById<ImageView>(Resource.Id.comment_respond_image_preview);
            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.comment_respond_layout);
            rl.AddView(row);
            ImageView pick_image_button = FindViewById<ImageView>(Resource.Id.comment_respond_image_add);
            pick_image_button.Click += pick_image;
            TextView send_button = FindViewById<TextView>(Resource.Id.add_comment_respond_button);
            send_button.Click += add_comment;
            TextView delete_respond_image = FindViewById<TextView>(Resource.Id.comment_respond_preview_delete);
            delete_respond_image.Click += delegate {
                LinearLayout rl1 = FindViewById<LinearLayout>(Resource.Id.comment_respond_image_preview_layout);
                rl1.Visibility = ViewStates.Gone;
                App.bitmap = null;
                App.video = null;
                giphy_image = "";
                giphy_video = "";
            };
        }
        
        private void add_comment(object sender, EventArgs e)
        {            
            TextView empty = FindViewById<TextView>(Resource.Id.comment_respond_empty);
            empty.Visibility = ViewStates.Gone;
            string token = UserParameters.retrieveset("token");
            EditText comment_text = FindViewById<EditText>(Resource.Id.comment_respond_text);
            if (token == null) return;
            if (comment_text.Text == "" && App.bitmap == null && giphy_image=="" && App.video == null)
            {
                empty.Visibility = ViewStates.Visible;
                return;
            }
            Comment.add_comment(token, post_id, comment_id, comment_text.Text, App.bitmap,App.video, giphy_image,giphy_video);
            Finish();
            //System.Console.WriteLine(UploadBitmapAsync(App.bitmap));
        }

       

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                if (requestCode == REQUEST_CAMERA)
                {
                    // Handle the newly captured image
                    Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                    Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
                    mediaScanIntent.SetData(contentUri);
                    SendBroadcast(mediaScanIntent);

                    // Display in ImageView. We will resize the bitmap to fit the display.
                    // Loading the full sized image will consume to much memory
                    // and cause the application to crash.

                    //int height = Resources.DisplayMetrics.HeightPixels;
                    ImageView img_preview = FindViewById<ImageView>(Resource.Id.comment_respond_image_preview);
                    int width = 720;
                    App.bitmap = App._file.Path.LoadAndResizeBitmap(width);
                    if (App.bitmap != null)
                    {
                        img_preview_layout.Visibility = ViewStates.Visible;
                        video_preview.Visibility = ViewStates.Gone;
                        img_preview.Visibility = ViewStates.Visible;
                        int h =(int) App.bitmap.Height * 80 / App.bitmap.Width;
                        img_preview.SetImageBitmap(Bitmap.CreateScaledBitmap(App.bitmap,80,h,false));
                        App.bitmap = null;
                        giphy_image = "";
                        giphy_video = "";
                    }

                    // Dispose of the Java side bitmap.
                    GC.Collect();
                }
                else if (requestCode == SELECT_FILE)
                {
                    // Handle the image chosen from gallery
                    
                    Android.Net.Uri uri = data.Data;
                    img_preview_layout.Visibility = ViewStates.Visible;
                    video_preview.Visibility = ViewStates.Gone;
                    img_preview.Visibility = ViewStates.Visible;
                    App.bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, uri);
                    img_preview.SetImageURI(uri);
                    giphy_image = "";
                    giphy_video = "";
                }
                else if (requestCode == GIPHY_SEARCH)
                {
                    if (data.HasExtra("image"))
                    {
                        App.bitmap = null;
                        video_preview.SetVideoURI(Android.Net.Uri.Parse(data.GetStringExtra("video")));
                        video_preview.Start();
                        video_preview.Visibility = ViewStates.Visible;
                        img_preview.Visibility = ViewStates.Gone;
                        img_preview_layout.Visibility = ViewStates.Visible;
                        Koush.UrlImageViewHelper.SetUrlDrawable(img_preview, giphy_image);
                        giphy_image = data.GetStringExtra("image");
                        giphy_video = data.GetStringExtra("video");
                    }
                }else if (requestCode == SELECT_VIDEO)
                {
                    Android.Net.Uri uri = data.Data;
                    img_preview_layout.Visibility = ViewStates.Visible;                    
                    video_preview.Visibility = ViewStates.Visible;
                    img_preview.Visibility = ViewStates.Gone;
                    video_preview.SetVideoURI(uri);
                    video_preview.SeekTo(100);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    FileInputStream fis;
                    try
                    {
                        Java.IO.File file = new Java.IO.File(PostView.GetRealPathFromURI(this, uri));
                        fis = new FileInputStream(file);

                        byte[] buf = new byte[1024];
                        int n;
                        while (-1 != (n = fis.Read(buf)))
                            baos.Write(buf, 0, n);
                    }
                    catch (Exception e)
                    {

                    }
                    byte[] bbytes = baos.ToByteArray();
                    App.video = Convert.ToBase64String(bbytes);
                    App.bitmap = null;
                    giphy_image = "";
                }
            }
        }
        private static readonly Int32 REQUEST_CAMERA = 0;
        private static readonly Int32 SELECT_FILE = 1;
        private static readonly Int32 GIPHY_SEARCH = 2;
        private static readonly Int32 SELECT_VIDEO = 3;
        private void pick_image(object sender, EventArgs e)
        {
            String[] items = { "Take Photo", "Choose from Library", "Load from Giphy or Imgur","Popular images" , "Upload video",  "Cancel" };

            using (var dialogBuilder = new AlertDialog.Builder (this)) {
                dialogBuilder.SetTitle("Add Photo");
                dialogBuilder.SetItems (items, (d, args) => {
                    //Take photo
                    if (args.Which == 0)
                    {
                        PostView.CreateDirectoryForPictures();
                        var intent = new Intent(MediaStore.ActionImageCapture);
                        App._file = new Java.IO.File(App._dir, string.Format("karma_{0}.jpg", Guid.NewGuid()));
                        intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
                        this.StartActivityForResult (intent, REQUEST_CAMERA);
                    }
                    //Choose from gallery
                    else if (args.Which == 1)
                    {                        
                        var intent = new Intent(Intent.ActionPick, MediaStore.Images.Media.ExternalContentUri);
                        intent.SetType ("image/*");
                        this.StartActivityForResult (Intent.CreateChooser (intent, "Select Picture"), SELECT_FILE);
                    }
                    else if (args.Which == 2)
                    {
                        var intent = new Intent(this, typeof(GiphySearch));
                        this.StartActivityForResult(intent, GIPHY_SEARCH);
                    }
                    else if (args.Which == 3)
                    {
                        var intent = new Intent(this, typeof(UserImages));
                        this.StartActivityForResult(intent, GIPHY_SEARCH);
                    }
                    else if(args.Which == 4)
                    {
                        var intent = new Intent(Intent.ActionPick, MediaStore.Video.Media.ExternalContentUri);
                        intent.SetType("video/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Video"), SELECT_VIDEO);
                    }
                });

                dialogBuilder.Show ();
            }
        }

        private Comment getComment(string comment_id)
        {
            string url = "http://exifeed.com/api.php/comment/"+comment_id+ "?api_key=sdsgre345345hkb5kh345kj345";
            WebClient webClient = new WebClient();
            Uri uri = new Uri(url);
            string json = Encoding.UTF8.GetString(webClient.DownloadData(uri));
            Comment com = Newtonsoft.Json.JsonConvert.DeserializeObject<Comment>(json);
            return com;
        }

        private ImageView CreatePlayButton(Context context, string video)
        {
            ImageView im = new ImageView(context);
            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(80, 80);
            im.LayoutParameters = p;
            p.AddRule(LayoutRules.CenterInParent);
            im.SetAdjustViewBounds(true);
            im.SetImageResource(Resource.Drawable.play_icon);
            im.Click += delegate
            {
                ImageClick(context, video);
            };
            return im;
        }

        private void ImageClick(Context context, string video)
        {
            Intent intent = new Intent(context, typeof(Video));
            intent.PutExtra("video", video);
            context.StartActivity(intent);
        }
        public static class App
        {
            public static Java.IO.File _file;
            public static Java.IO.File _dir;
            public static Bitmap bitmap;
            public static string video;
        }
        
    }

    public static class BitmapHelpers
    {
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            int height = options.OutHeight * (options.OutWidth / width);
            if (outHeight > height || outWidth > width)
            {
               // inSampleSize = outWidth > outHeight
                 //                  ? outHeight / height
                   //                : outWidth / width;
            }
            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = options.OutWidth / width;
            
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);
            return resizedBitmap;
        }

        public static Bitmap crop(Bitmap bitmap)
        {
            int x = 0;int y = 0;
            int l = 0;
            if (bitmap.Width > bitmap.Height)
            {
                x = (int)(bitmap.Width - bitmap.Height) / 2;
                l = bitmap.Height;
            }else
            {
                y = (int)(bitmap.Height - bitmap.Width) / 2;
                l = bitmap.Width;
            }
            Bitmap new_bitmap = Bitmap.CreateBitmap(bitmap, x, y, l, l);
            return new_bitmap;
        }
    }
}
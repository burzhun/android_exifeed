using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Specialized;

namespace AndroidGesture
{
    [Activity(Label = "GiphySearch")]
    public class GiphySearch : Activity
    {
        ListView listview;
        VideoView videoview;
        ImageView imageview;
        GiphyAdapter adapter;
        int lastposition = 0;
        int offset = 0;
        int i = 0;
        View row1;
        List<GiphySearchResult> list;
        string text;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.giphy);

            EditText textview = FindViewById<EditText>(Resource.Id.giphy_text);
            TextView button = FindViewById<TextView>(Resource.Id.giphy_button);
            listview = FindViewById<ListView>(Resource.Id.giphy_list);
            listview.Scroll += scroll;
             listview.ItemClick += click;
           
            button.Click += (o, e) =>
            {
                text = textview.Text;
                if (text == "") return;
                Uri address = new Uri("http://www.exifeed.com/api.php/giphy_search?api_key=sdsgre345345hkb5kh345kj345");
                var webClient = new System.Net.WebClient();
                NameValueCollection nameValueCollection = new NameValueCollection();
                nameValueCollection["query"] = text;
                byte[] result = webClient.UploadValues(address, "POST", nameValueCollection);
                try
                {
                    string json = Encoding.UTF8.GetString(result);
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GiphySearchResult>>(json);
                    adapter = new GiphyAdapter(list, this);
                    listview.Adapter = adapter;
                }
                catch (Exception e1)
                {
                    return;
                }
            };
            
        }

        private void click(object sender, AdapterView.ItemClickEventArgs e)
        {
            string url = list[e.Position].video;
            if (url == "") return;
            Intent intent = new Intent(this, typeof(Video)).SetFlags(ActivityFlags.ClearTop);
            try
            {
                ImageView image = listview.GetChildAt(e.Position).FindViewById<ImageView>(Resource.Id.giphy_image);
                int height = image.Height;
                int width = image.MeasuredWidth;                
                intent.PutExtra("width", width);
                intent.PutExtra("height", height);
            }catch(Exception e1)
            { }
            // TextView textview = listview.GetChildAt(e.Position).FindViewById<TextView>(Resource.Id.giphy_add_button);
            intent.PutExtra("video", url);
            this.StartActivity(intent);

           
        }

        private void scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (list == null) return;
            if(e.View.LastVisiblePosition >= list.Count - 1)
            {
                offset += list.Count;
                Uri address = new Uri("http://www.exifeed.com/api.php/giphy_search?api_key=sdsgre345345hkb5kh345kj345&offset="+offset.ToString());
                var webClient = new System.Net.WebClient();
                NameValueCollection nameValueCollection = new NameValueCollection();
                nameValueCollection["query"] = text;
                byte[] result = webClient.UploadValues(address, "POST", nameValueCollection);
                try
                {
                    string json = Encoding.UTF8.GetString(result);
                    List<GiphySearchResult> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GiphySearchResult>>(json);
                    list.AddRange(list2);
                    adapter.NotifyDataSetChanged();
                }
                catch (Exception e1)
                {
                    return;
                }
            }
            
        }
    }
}
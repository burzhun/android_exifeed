using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using Android.Util;
using Android.Graphics;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using Java.Lang;
using Xamarin.Facebook.Login;
using Org.Json;
using Android.Provider;
using System.IO;

namespace AndroidGesture
{
    [Activity(Label = "Settings")]
    public class SettingsActivity : Activity
    {
        ImageView userimage;
        TextView update_image;
        TextView delete_image;
        TextView set_image;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.settings);
            if (UserParameters.retrieveset("token") == null|| UserParameters.retrieveset("username")==null)
            {
                Finish();
            }
			EditText username = FindViewById<EditText>(Resource.Id.settings_username);
			TextView save_button = FindViewById<TextView>(Resource.Id.settings_save);
			userimage = FindViewById<ImageView>(Resource.Id.settings_userimage);
			set_image = FindViewById<TextView>(Resource.Id.settings_addimage);
			update_image = FindViewById<TextView>(Resource.Id.settings_updateimage);
			delete_image = FindViewById<TextView>(Resource.Id.settings_deleteimage);
            CheckBox shorten_post = FindViewById<CheckBox>(Resource.Id.shorten_post);
			EditText old_password = FindViewById<EditText>(Resource.Id.old_password);
			EditText new_password = FindViewById<EditText>(Resource.Id.new_password);
			EditText new_password_confirmed = FindViewById<EditText>(Resource.Id.new_password_confirmed);
			TextView save_password = FindViewById<TextView>(Resource.Id.save_password);
			TextView password_error  = FindViewById<TextView>(Resource.Id.password_error);
            ImageView settings_loader = FindViewById<ImageView>(Resource.Id.settings_loader);
            TextView settings_error = FindViewById<TextView>(Resource.Id.settings_error);
            username.Text = UserParameters.retrieveset("username");
            Koush.UrlImageViewHelper.SetUrlDrawable(userimage, UserParameters.retrieveset("userimage"));
            if (UserParameters.retrieveset("userimage").Contains("no_image_user.png"))
            {
                set_image.Visibility = ViewStates.Visible;
            }else
            {
                update_image.Visibility = ViewStates.Visible;
                delete_image.Visibility = ViewStates.Visible;
            }
            shorten_post.Checked = UserParameters.retrieveset("shorten_post") == "1";
			save_password.Click += delegate{
				password_error.Visibility = ViewStates.Gone;
				if(new_password.Text != new_password_confirmed.Text){
					password_error.Text = "Old passwords should be the same";
					password_error.Visibility = ViewStates.Visible;
                    return;
				}
                var ar = new Dictionary<string,string>();
                ar.Add("old_password", old_password.Text);
                ar.Add("new_password", new_password.Text);
                string result = UrlManager.PostRequest("settings", ar);
			};
            save_button.Click += delegate {
                Console.WriteLine("click");
                settings_error.Visibility = ViewStates.Gone;
                string shorten = shorten_post.Checked ? "1" : "0";
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("username", username.Text);
                dict.Add("shorten_post", shorten);
                settings_loader.Visibility = ViewStates.Visible;
                string json = UrlManager.PostRequest("settings", dict);
                Console.WriteLine(json);
                settings_loader.Visibility = ViewStates.Gone;
                if(json == "saved")
                {
                    settings_error.Text = "Saved";
                    settings_error.SetTextColor(Color.Green);
                    settings_error.Visibility = ViewStates.Visible;
                    UserParameters.saveset("username", username.Text);
                    UserParameters.saveset("shorten_post", shorten);
                }
                if(json == "wrong length")
                {
                    settings_error.Text = "Username must contain form 3 to 25 characters";
                    settings_error.SetTextColor(Color.Red);
                    settings_error.Visibility = ViewStates.Visible;
                }
                if(json == "wrong username")
                {
                    settings_error.Text = "Username may contain only letters,digits,'_' and must start with letter";
                    settings_error.SetTextColor(Color.Red);
                    settings_error.Visibility = ViewStates.Visible;
                }

            };
            set_image.Click += Set_image;
            update_image.Click += Set_image;
            delete_image.Click += Delete_image;
        }

        private void Delete_image(object sender, EventArgs e)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("delete_image", "1");
            string json = UrlManager.PostRequest("settings", dict);
            Koush.UrlImageViewHelper.SetUrlDrawable(userimage, json);
            update_image.Visibility = ViewStates.Gone;
            delete_image.Visibility = ViewStates.Gone;
            set_image.Visibility = ViewStates.Visible;
        }

        public static class App
        {
            public static Java.IO.File _file;
            public static Java.IO.File _dir;
            public static Bitmap bitmap;
        }
        private static readonly Int32 REQUEST_CAMERA = 0;
        private static readonly Int32 SELECT_FILE = 1;
        private static readonly Int32 GIPHY_SEARCH = 2;
        private void Set_image(object sender, EventArgs e)
        {            
            string[] items = { "Take Photo", "Choose from Library", "Cancel" };

            using (var dialogBuilder = new AlertDialog.Builder(this))
            {
                dialogBuilder.SetTitle("Add Photo");
                dialogBuilder.SetItems(items, (d, args) => {
                    //Take photo
                    if (args.Which == 0)
                    {
                        PostView.CreateDirectoryForPictures();
                        var intent = new Intent(MediaStore.ActionImageCapture);
                        App._file = new Java.IO.File(App._dir, string.Format("karma_{0}.jpg", Guid.NewGuid()));
                        intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(App._file));
                        this.StartActivityForResult(intent, REQUEST_CAMERA);
                    }
                    //Choose from gallery
                    else if (args.Which == 1)
                    {
                        var intent = new Intent(Intent.ActionPick, MediaStore.Images.Media.ExternalContentUri);
                        intent.SetType("image/*");
                        this.StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), SELECT_FILE);
                    }
                });

                dialogBuilder.Show();
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                if (requestCode == REQUEST_CAMERA)
                {
                    // Handle the newly captured image
                    Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                    Android.Net.Uri contentUri = Android.Net.Uri.FromFile(App._file);
                    mediaScanIntent.SetData(contentUri);
                    SendBroadcast(mediaScanIntent);
                    
                    int width = 720;
                    App.bitmap = App._file.Path.LoadAndResizeBitmap(width);
                    if (App.bitmap != null)
                    {
                        Bitmap cropped = BitmapHelpers.crop(App.bitmap);
                        int h = (int)App.bitmap.Height * 200 / App.bitmap.Width;
                        userimage.SetImageBitmap(Bitmap.CreateScaledBitmap(cropped, 200, h, false));
                        byte[] bitmapData;
                        using (var stream = new MemoryStream())
                        {
                            App.bitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 0, stream);
                            bitmapData = stream.ToArray();
                        }
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        dict.Add("user_image", Convert.ToBase64String(bitmapData));
                        UrlManager.PostRequest("settings", dict);
                        App.bitmap = null;
                    }
                    
                    GC.Collect();
                }
                else if (requestCode == SELECT_FILE)
                {
                    // Handle the image chosen from gallery
                    Android.Net.Uri uri = data.Data;
                    App.bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, uri);
                    int width = App.bitmap.Width > 800 ? 800 : App.bitmap.Width;
                    int h = App.bitmap.Height * width / App.bitmap.Width;
                    Bitmap bitmap = Android.Graphics.Bitmap.CreateScaledBitmap(App.bitmap, width, h, false);
                    Bitmap cropped = BitmapHelpers.crop(bitmap);
                    userimage.SetImageBitmap(cropped);
                    byte[] bitmapData;
                    using (var stream = new MemoryStream())
                    {
                        cropped.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 0, stream);
                        bitmapData = stream.ToArray();
                    }
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("user_image", Convert.ToBase64String(bitmapData));
                    UrlManager.PostRequest("settings", dict);
                    App.bitmap = null;
                }
                set_image.Visibility = ViewStates.Gone;
                update_image.Visibility = ViewStates.Visible;
                delete_image.Visibility = ViewStates.Visible;
                Koush.UrlImageViewHelper.Cleanup(this);
                UserParameters.saveset("userimage_changed", "1");
            }
        }
    }
}
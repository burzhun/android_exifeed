using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.Net;

namespace AndroidGesture
{
    [Activity(Label = "CoubVideo", Theme = "@android:style/Theme.Translucent.NoTitleBar.Fullscreen")]
    public class CoubVideo : Activity
    {
        MediaPlayer mp;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.coub_video);
            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.coubLayout);           
            VideoView video1 = FindViewById<VideoView>(Resource.Id.coubVideo);
            String url = Intent.GetStringExtra("video");
            video1.SetVideoURI(Android.Net.Uri.Parse(url));            
            String audio = Intent.GetStringExtra("audio");
            mp = new MediaPlayer();
            bool playing = true;
            mp.SetAudioStreamType(Stream.Music);   
                    
            int video_elapsed = 0;
            int audio_elapsed = 0;
            mp.SetOnPreparedListener(new VideoLoop());
            
            
            
                mp.SetDataSource(this, Android.Net.Uri.Parse(audio));
                mp.PrepareAsync();
                mp.Prepared += delegate
                {
                    video1.SetVideoURI(Android.Net.Uri.Parse(url));
                    video1.Start();
                    mp.Start();
                    
                };             
               // video1.Start();
                          
           
            video1.Completion += delegate
            {
               // mp.Prepare();
                
            };
            video1.Touch += delegate
            {
                if (playing)
                {
                    video1.Pause();
                    mp.Pause();
                    video_elapsed = video1.CurrentPosition;
                    audio_elapsed = mp.CurrentPosition;
                }
                else
                {
                    video1.SeekTo(video_elapsed);
                    mp.SeekTo(audio_elapsed);
                }
                playing = !playing;
                
            };
            rl.Click += delegate
            {
                Finish();
            };

        }

        public void onInfo(Android.Media.MediaPlayer.InfoEventArgs l)
        {
            
        }
        protected override void OnStop()
        {
            base.OnStop();
            mp.Stop();
            mp.Release();
        }
        

    }

}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidGesture
{
    [Activity(Label = "MyComments")]
    public class LastComments : Activity
    {
        List<Comment> list;
        ListView list_com;
        CommentAdapter adapter;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.my_comments);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            // Create your application here
            list_com = FindViewById<ListView>(Resource.Id.mycommentsList);
            list_com.Scroll += com_scroll;
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("token", UserParameters.retrieveset("token"));
            string json = UrlManager.PostRequest("lastcomments", dict);
            try
            {
                if (json != "")
                {
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Comment>>(json);
                    adapter = new CommentAdapter(list, this, true);
                    list_com.Adapter = adapter;
                    list_com.ItemClick += comment_click;
                }
                else
                {
                    ShowMessage("No comments were found :(");
                }
            }
            catch (Exception e)
            {
                ShowMessage("There has been some error :(");
            }

        }

        private void com_scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (list == null || list.Count == 0) return;
            if (e.View.LastVisiblePosition + 1 == list.Count)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("token", UserParameters.retrieveset("token"));
                dict.Add("offset", list.Count.ToString());
                string json = UrlManager.PostRequest("lastcomments", dict);
                try
                {
                    List<Comment> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Comment>>(json);
                    list.AddRange(list2);
                    adapter.NotifyDataSetChanged();
                }
                catch (Exception e1)
                {
                    ShowMessage("There has been some error :(");
                }
            }
        }

        private void comment_click(object sender, AdapterView.ItemClickEventArgs e)
        {
            // if (e.Position == 0) return;
            Intent intent = new Intent(this, typeof(CommentAction)).SetFlags(ActivityFlags.ClearTop);
            intent.PutExtra("comment_id", list[e.Position].id);
            intent.PutExtra("post_id", list[e.Position].post_id);
            StartActivityForResult(intent, 1);
        }

        public void ShowMessage(string message)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(message);
            alert.SetPositiveButton("Ok", (senderAlert, args) => {
            });
            RunOnUiThread(() => {
                alert.Show();
            });
        }
    }
}
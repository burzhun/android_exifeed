using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using Android.Media;

namespace AndroidGesture
{
    [Activity(Label = "GiphyAdapter")]
    public class GiphyAdapter : BaseAdapter<GiphySearchResult>
    {
        public List<GiphySearchResult> list;
        public Context context;
        int number = -1;
        public GiphyAdapter(List<GiphySearchResult> list1, Context context1)
        {
            list = list1;
            context = context1;
        }
        public override GiphySearchResult this[int position]
        {
            get
            {
                return list[position];
            }
        }

        public override int Count
        {
            get
            {
                return list.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }
        
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            GiphySearchResult elem = list[position];
            if (row == null)
            {
                row = LayoutInflater.From(context).Inflate(Resource.Layout.giphy_search, null, false);
            }
            TextView textview = row.FindViewById<TextView>(Resource.Id.giphy_add_button);
            textview.SetOnClickListener(new ButtonClickListener(elem.image, elem.video, context));            
            ImageView giphy_play_button = row.FindViewById<ImageView>(Resource.Id.giphy_play_button);
            giphy_play_button.Visibility = ViewStates.Gone;
            ImageView image = row.FindViewById<ImageView>(Resource.Id.giphy_image);
            Koush.UrlImageViewHelper.SetUrlDrawable(image, elem.image);
            if (elem.video != "")
            {
                giphy_play_button.Visibility = ViewStates.Visible;
            }
                  
            return row;
        }

    }

    public class GiphySearchResult
    {
        public string image;
        public string video;
       
        public GiphySearchResult(string Image, string Video)
        {
            image = Image;
            video = Video;            
        }
    }

    public class ButtonClickListener : Java.Lang.Object, View.IOnClickListener
    {
        string image;
        string video;
        Context c;
        public ButtonClickListener(string image1,string video1, Context context)
        {
            image = image1;
            video = video1;
            c = context;
        }
        public void OnClick(View v)
        {
            Intent intent = new Intent();
            intent.PutExtra("video",video);
            intent.PutExtra("image", image);
            ((Activity)c).SetResult(Result.Ok, intent);
            ((Activity)c).Finish();
        }
    }
}
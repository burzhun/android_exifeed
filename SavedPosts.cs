using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Exifeed;

namespace AndroidGesture
{
    [Activity(Label = "SavedPosts")]
    public class SavedPosts : Activity
    {
        ListView listview;
        List<Post> list;
        Post_adapter adapter;
        TextView textView;
        int offset = 0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.RequestFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.saved_posts);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            // Create your application here
            listview = FindViewById<ListView>(Resource.Id.savedpostListView);
            textView = FindViewById<TextView>(Resource.Id.saved_posts_caption);
            //listview.AddHeaderView(textView);
            listview.Scroll += scroll;
            try
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("type", "saved posts");
                string json = UrlManager.PostRequest("posts", dict);
                if(json!="" && json != "error")
                {
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                    adapter = new Post_adapter(list, this);
                    listview.Adapter = adapter;
                    offset += 10;
                }
                
            }
            catch(Exception e)
            { }
            

        }

        private void scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (offset>0 && e.FirstVisibleItem + e.TotalItemCount + 1 >= offset)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("type", "saved posts");
                dict.Add("offset", offset.ToString());
                string json = UrlManager.PostRequest("posts", null);
                list.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json));
                adapter.NotifyDataSetChanged();
                offset += 10;
            }
        }
    }
}
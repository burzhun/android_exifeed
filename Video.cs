using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.Threading.Tasks;
using System.Threading;

namespace AndroidGesture
{
    [Activity(Label = "Video", Theme ="@android:style/Theme.Translucent.NoTitleBar.Fullscreen")]
    public class Video : Activity
    {
        int seek;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.video);
            string url = Intent.GetStringExtra("video");           
            VideoView video1 = FindViewById<VideoView>(Resource.Id.videoView1);
            if (Intent.HasExtra("width"))
            {
                int width = Intent.GetIntExtra("width",0);
                int height = Intent.GetIntExtra("height",0);
                if (width * height != 0)
                {
                    height = height * (width / 400);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(400,height);
                    video1.LayoutParameters = p;
                    p.AddRule(LayoutRules.CenterInParent);
                }
            }else
            {

            }
            video1.SetVideoURI(Android.Net.Uri.Parse(url));
            video1.Start();
            RelativeLayout rl = FindViewById<RelativeLayout>(Resource.Id.overlayLayout);
            bool playing = true;
            video1.SetOnPreparedListener(new VideoLoop());
            video1.Touch += delegate
            {
                if (playing)
                {
                    seek = video1.CurrentPosition;
                    video1.Pause();
                    Task task = Task.Run(()=>
                    {
                        Thread.Sleep(300);
                        playing = false;
                    });
                }
                else
                {
                    video1.SeekTo(seek);
                    video1.Start();
                    Task task1 = Task.Run(() =>
                    {
                        Thread.Sleep(300);
                        playing = true;
                    });
                }
            };
            rl.Click += delegate
            {                
                Finish();
            };
        }
    }

    public class VideoLoop : Java.Lang.Object, Android.Media.MediaPlayer.IOnPreparedListener
    {
        public void OnPrepared(MediaPlayer mp)
        {
            mp.Looping = true;

        }
    }
}
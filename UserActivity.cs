using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using Exifeed;

namespace AndroidGesture
{
    [Activity(Label = "UserActivity", Theme = "@style/AppTheme")]
    public class UserActivity : Activity
    {
        string user_id;
        string api;
        string token;
        int offset;
        int limit;
        WebClient webClient;
        ListView listview;
        Post_adapter adapter;
        List<Post> list;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.user);
            this.RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            // Create your application here
            api = "sdsgre345345hkb5kh345kj345";
            offset = 0;
            limit = 10;
            webClient = new WebClient();
            user_id = Intent.GetStringExtra("user_id");
            string url_user = "http://www.exifeed.com/api.php/user/"+user_id+"/?api_key=" + api;
            listview = FindViewById<ListView>(Resource.Id.user_postList);
            listview.Scroll += list_scrolled;
            token = UserParameters.retrieveset("token");
            if (token != null)
            {
                url_user += "&token=" + token;
            }
            Uri userToken = new Uri(url_user);
            webClient.DownloadDataAsync(userToken);
            webClient.DownloadDataCompleted += show_user;
            
        }

        private void list_scrolled(object sender, AbsListView.ScrollEventArgs e)
        {
            if (e.View.LastVisiblePosition+1 == offset + limit)
            {
                string json = DownloadPosts(user_id, offset + limit);
                List<Post> list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
                list.AddRange(list2);
                adapter = new Post_adapter(list, this);
                adapter.NotifyDataSetChanged();
                offset += limit;
            }
        }

        private void show_user(object sender, DownloadDataCompletedEventArgs e)
        {
            string json = Encoding.UTF8.GetString(e.Result);
            var jobject = Newtonsoft.Json.Linq.JObject.Parse(json);
            View row = LayoutInflater.From(this).Inflate(Resource.Layout.user_layout, null, false);
            ImageView iv = row.FindViewById<ImageView>(Resource.Id.user_layout_image);
            Koush.UrlImageViewHelper.SetUrlDrawable(iv, jobject["image"].ToString());
            TextView tv1 = row.FindViewById<TextView>(Resource.Id.user_layout_name);
            tv1.Text = jobject["username"].ToString();
            TextView tv2 = row.FindViewById<TextView>(Resource.Id.user_layout_info);
            tv2.Text = jobject["userinfo"].ToString().Split(',')[0];
            TextView tv4= row.FindViewById<TextView>(Resource.Id.user_layout_info3);
            tv4.Text = jobject["userinfo"].ToString().Split(',')[1];
            TextView tv3 = row.FindViewById<TextView>(Resource.Id.user_layout_info2);
            tv3.Text = jobject["posts_info"].ToString();
            Button subscribe = row.FindViewById<Button>(Resource.Id.subscribe_button);
            Button unsubscribe = row.FindViewById<Button>(Resource.Id.unsubscribe_button);
            listview.AddHeaderView(row);            
            json = DownloadPosts(user_id);
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Post>>(json);
            adapter = new Post_adapter(list, this);
            listview.Adapter = adapter;
            int subscribed = jobject["subscribed"].ToObject<int>();
            if(subscribed == 0)
            {
                subscribe.Visibility = ViewStates.Visible;
            }else if(subscribed == 1){
                unsubscribe.Visibility = ViewStates.Visible;
            }
            subscribe.Click += delegate
            {
                if (Subscribe(user_id))
                {
                    unsubscribe.Visibility = ViewStates.Visible;
                    subscribe.Visibility = ViewStates.Gone;
                }
            };
            unsubscribe.Click += delegate
            {
                if (Subscribe(user_id,true))
                {
                    subscribe.Visibility = ViewStates.Visible;
                    unsubscribe.Visibility = ViewStates.Gone;
                }
            };
        }

        private string DownloadPosts(string user_id,int offset1 = 0, int limit1 = 10)
        {            
            WebClient webClient = new WebClient();
            token = UserParameters.retrieveset("token");
            string urls = "http://www.exifeed.com/api.php/post_user/"+user_id+"?api_key=" + api;
            if (token != null)
            {
                urls += "&token=" + token;
            }
            if (offset1 != 0)
            {
                urls += "&offset=" + offset1.ToString();
            }

            if (limit1 != 10)
            {
                urls += "&limit=" + limit1.ToString();
            }
            Uri url = new Uri(urls);
            return Encoding.UTF8.GetString(webClient.DownloadData(url));            

        }

        private bool Subscribe(string user_id,bool unsubscribe=false)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("user_id", user_id);
            if (unsubscribe)
            {
                dict.Add("unsubscribe", "1");
            }
            return UrlManager.PostRequest("subscribe", dict) == "ok";
        }
    }
}